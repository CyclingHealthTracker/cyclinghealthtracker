import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticateUserService } from '../services/AuthenticateUser.service';
//adds token to all internal API calls
//for external API calls we will use the external calls service
@Injectable()
export class JSONwebTokenInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticateUserService) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser.token) {
            request = request.clone({
                setHeaders: { //sets the header of the HTTP request with the currentUser.Token 
                    Authorization: `Bearer ${currentUser.token}`
                }
            });
        }

        return next.handle(request);
    }
}