import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './helpers/auth-guard.guard';
import { DashBoard } from './components/dashboard/DashBoard.component';
import { RegisterComponent } from './components/register/register.component';
import { ActivityLoggingComponent } from './components/activity-logging/activity-logging.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { RegisterRedirectComponent } from './components/register-redirect/register-redirect.component';
import { FitBitRegisterRedirectComponent } from './components/fit-bit-register-redirect/fit-bit-register-redirect.component'
import { ClientViewComponent } from './components/client-view/client-view.component';
import { CoachingHandlerComponent } from './components/coaching-handler/coaching-handler.component';



const routes: Routes = [

  { path: '', component: DashBoard, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'redirect', component: RegisterRedirectComponent },
  { path: 'fitbitRedirect', component: FitBitRegisterRedirectComponent },
  { path: 'activityLogging', component: ActivityLoggingComponent },
  { path: 'schedule', component: ScheduleComponent },
  { path: 'clientView', component: ClientViewComponent },
  { path: 'coachingHandler', component: CoachingHandlerComponent },



  { path: '**', redirectTo: '' }

];

export const AppRoutingModule = RouterModule.forRoot(routes);