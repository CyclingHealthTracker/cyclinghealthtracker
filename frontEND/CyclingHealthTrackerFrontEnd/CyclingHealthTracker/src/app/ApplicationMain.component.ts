//app main component 
//This component serves the Typescript for the main header



import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { User } from './models/user';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserAPIService } from 'src/app/services/user.service';

@Component({ selector: 'app', templateUrl: 'applicationMain.html' })

export class ApplicationMainComponent implements OnInit{

    currentUser: User;
    userRole: string;
    isHiddenVal: boolean;
    isHiddenFB: boolean;
    isHiddenStrava: boolean;
    firstName: string = '';
    emailAddress: string = '';
    lastName: string;
    rrorMessage: string = null;
    SuccessResponse: string = null;
    errorMessage: string = null;

    constructor(
        private router: Router,
        private userService: UserAPIService,
        private formBuilder: FormBuilder, //adding formBuilder into constructor
        private authenticationService: AuthenticateUserService
    ) {

        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

    }
//initial form data for validation against the NG disabled configurations
    ResetPasswordForm = this.formBuilder.group({

        CurrPassword: new FormControl('',Validators.required),
        password: new FormControl('', Validators.required),
        confirmPassword: new FormControl('',Validators.required),
        });
    
         GeneralProfileForm = this.formBuilder.group({
    
       FirstName: new FormControl('', Validators.required),
       LastName: new FormControl('', Validators.required),
       emailAddress: new FormControl('', Validators.required),
        });
    
        AthleteProfileForm = this.formBuilder.group({
    
    
      Weight: new FormControl('',Validators.required),
      Height: new FormControl('',Validators.required),
      age: new FormControl('', Validators.required),
    
      });
    

      athEventChange: boolean = false;
      generalChange: boolean = false;

      selectedChangeHandler (event: any){

        this.athEventChange = true;
        console.log('athlete change handler', this.athEventChange)

    }

    GeneralSelectedChangeHandler (event: any){

        this.generalChange = true;
        console.log('general change handler', this.generalChange)

    }
        


    ngOnInit(){

if(this.currentUser)

{



//nav items depending on user type
        if(this.currentUser.userRole == 'Coach'){    //checking for user role for nav items
            this.isHiddenVal = true
            console.log('hidden logic, ture is coach ---', this.isHiddenVal)
        }
        if(this.currentUser.userRole == 'Athlete'){   //checking for user role for nav items
            this.isHiddenVal = false
            console.log('hidden logic, ture is coach ---', this.isHiddenVal)
        }
        if(this.currentUser.stravaRefreshToken == ''){   //checking for user role for nav items
            this.isHiddenStrava = true
            console.log('hidden logic, strava refresh token is missing ---', this.isHiddenStrava)

        }
        if(this.currentUser.fitbitRefreshToken == ''){   //checking for user role for nav items
            this.isHiddenFB = true
            console.log('hidden logic, fitbit refresh token is missing ---', this.isHiddenFB)

        }

    }


    }


    //redirectURI = http://localhost:4200/fitbitRedirect

    fitBitCall(){

        window.location.href = 'https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=22BDKJ&redirect_uri=http://localhost:4200/fitbitRedirect&scope=activity%20nutrition%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight'

    }

    patchGenValues(){

        this.GeneralProfileForm.patchValue({FirstName: this.currentUser.firstName, LastName: this.currentUser.lastName, emailAddress: this.currentUser.emailAddress})
    
    }

    viewProfile: boolean
    userProfile(){

this.viewProfile = true
//getProfile request for logged in user

//we patch the values here as they are not initialized on startup
this.patchGenValues();


//path the values for the athlete frofile
this.AthleteProfileForm.patchValue({Weight: this.currentUser.weight, Height: this.currentUser.height, age: this.currentUser.age, CoachEmail: this.currentUser.coachEmail})
    }

    stravaCall(){
        window.location.href = 'https://strava.com/oauth/authorize?response_type=code&client_id=37777&redirect_uri=http://localhost:4200/redirect&scope=activity:read' //this directs the user to the strava authentication page, in return the browser then redirects back to the CHT
        
        }

    logout() {
        this.authenticationService.logoutUser();
        this.currentUser = null
        this.router.navigate(['/login']);
    }

//profile handler
//handing all profile requests from the top right hand corner of home screen
//GeneralProfileForm


//update handler

editGeneralProfile()   

{

    var formData: any = new FormData();  
    formData.append("UserName", this.currentUser.username);
    formData.append("FirstName", this.GeneralProfileForm.get('FirstName').value);
    formData.append("LastName", this.GeneralProfileForm.get('LastName').value);
    formData.append("emailAddress", this.GeneralProfileForm.get('emailAddress').value);


    //setting new vars for the local storage based on a successful repsonse

    var newFirstName = this.GeneralProfileForm.get('FirstName').value
    var newLastName =  this.GeneralProfileForm.get('LastName').value
    var newEmailAddress = this.GeneralProfileForm.get('emailAddress').value

this.userService.postUserRoleUpdate(formData).subscribe(

    (response) => {
       console.log(response['value'])
        this.errorMessage = null;
        this.SuccessResponse = response['value']; 

        setTimeout(() => {
            this.SuccessResponse = null;  //performance dependency, please do not remove - dmcfall
//setting timeout to handle request completion
        }, 800);

        //within this we have updated the firstName, Lastname and email address, so we will update the local sotrage var
        //instead of doing another server request
        var NewCurrentUser = JSON.parse(localStorage.getItem("currentUser"))
        NewCurrentUser.firstName = newFirstName;
        NewCurrentUser.lastName = newLastName;
        NewCurrentUser.emailAddress = newEmailAddress;

        localStorage.setItem("currentUser",JSON.stringify(NewCurrentUser));

        this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.GeneralProfileForm.reset();
        this.GeneralProfileForm.patchValue({FirstName: this.currentUser.firstName, LastName: this.currentUser.lastName, emailAddress: this.currentUser.emailAddress})

        this.generalChange = false; //setting bool to false so no changes are present and form disables again
        
   },
    (error) => {
  
      this.SuccessResponse = null;
        this.errorMessage = error; 
        //this.alertService.error(error);
        console.log(error); //logging error to console for debugging purposes. 
  
    }
  );
  }

  editAthleteDetails()     //edit athlete post request handler
  {
  
      var formData: any = new FormData();  
      formData.append("UserName", this.currentUser.username);
      formData.append("Age", this.AthleteProfileForm.get('age').value);
      formData.append("Weight", this.AthleteProfileForm.get('Weight').value);
      formData.append("Height", this.AthleteProfileForm.get('Height').value);
  
      var newAge = this.AthleteProfileForm.get('age').value
      var newWeight = this.AthleteProfileForm.get('Weight').value
      var newHeight = this.AthleteProfileForm.get('Height').value

  this.userService.postAthleteUpdate(formData).subscribe(
  
      (response) => {
         console.log(response['value'])
          this.errorMessage = null;
          this.SuccessResponse = response['value']; 
          setTimeout(() => {
            this.SuccessResponse = null;  //performance dependency, please do not remove - dmcfall
//setting timeout to handle request completion
        }, 800);

        //within this we have updated the firstName, Lastname and email address, so we will update the local sotrage var
        //instead of doing another server request
        var NewCurrentUser = JSON.parse(localStorage.getItem("currentUser"))
        NewCurrentUser.age = newAge;
        NewCurrentUser.weight = newWeight;
        NewCurrentUser.height = newHeight;

        localStorage.setItem("currentUser",JSON.stringify(NewCurrentUser));

        this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.GeneralProfileForm.reset();
        this.GeneralProfileForm.patchValue({FirstName: this.currentUser.firstName, LastName: this.currentUser.lastName, emailAddress: this.currentUser.emailAddress})

        this.athEventChange = false; //setting bool to false so no changes are present and form disables again
        
      },
      (error) => {
    
        this.SuccessResponse = null;
          this.errorMessage = error; 
          //this.alertService.error(error);
          console.log(error);
    
      }
    );
    }


passwordReset()   //method to handle a password reset within the application
{


    var formData: any = new FormData();  
    formData.append("UserName", this.currentUser.username);
    formData.append("CurrentPassword", this.ResetPasswordForm.get('CurrPassword').value);
    formData.append("NewPassword", this.ResetPasswordForm.get('password').value);
    formData.append("ConfirmNewPassword", this.ResetPasswordForm.get('confirmPassword').value);


this.userService.postPasswordRest(formData).subscribe(

    (response) => {
       console.log(response['value'])
        this.errorMessage = null;
        this.SuccessResponse = response['value']; 
        
    },
    (error) => {
  
      this.SuccessResponse = null;
        this.errorMessage = error; 
        //this.alertService.error(error);
        console.log(error);
  
    }
  );
  }

}









