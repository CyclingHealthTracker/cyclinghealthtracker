import { Injectable } from '@angular/core';
import {HttpBackend, HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import { map } from 'rxjs/operators';
import { StravaActivity } from '../models/strava-activity';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { Fitbit } from '../models/fitbit';


@Injectable({
  providedIn: 'root'
})
export class ExternalAPIcallsService {
  currentUser: any;

  constructor(private http: HttpClient, handler: HttpBackend ,
    private authenticationService: AuthenticateUserService,
    ) { 

    this.http = new HttpClient(handler);
    this.currentUser = this.authenticationService.currentUserValue;

  }


loadFitbit(){

  let FitbitToken = localStorage.currentUserfitbitAccessToken
  let FitbitUserID = localStorage.currentUserfitbitUserID


  const headersFB = new HttpHeaders() 
               .set('accept', 'application/json') //Strava API protocol to accept application/json
               .set('Authorization', 'Bearer ' + FitbitToken);   //using the token from the current user local storage

//GET request for sleep FITBIT --- GET https://api.fitbit.com/1.2/user/6W9Y6V/sleep/date/[date].json

//https://api.fitbit.com/1.2/user/[user-id]/sleep/date/[startDate]/[endDate].json -- this request is for a date range
//example date range --- 2017-04-02/2017-04-08
//test user 6W9Y6V

return this.http.get<[Fitbit]>('https://api.fitbit.com/1.2/user/6W9Y6V/sleep/date/2019-10-02/2020-01-05.json',{ headers: headersFB }).pipe(map(data => data ['sleep']))   //using pipe to map into the nested JSOn from FITBIT

}
  loadStravaActivities()
  {
    let currentUserDataStr = localStorage.getItem("currentUser"); 
    let currentUserData = JSON.parse(currentUserDataStr);
    let token = currentUserData.stravaAccessToken

    
           const headers = new HttpHeaders()  //declaring the header needed for the HTTP RQ to Strava API
               .set('accept', 'application/json') //Strava API protocol to accept application/json
               .set('Authorization', 'Bearer ' + token);   //using the token from the current user local storage
  

               console.log('request to strava, header  --- ',headers) //logginf for debugging and monitoring

           return this.http.get<[StravaActivity]>('https://www.strava.com/api/v3/athlete/activities?per_page=10',{ headers: headers })

  } 
  
getRefreshFromFitbit(FitbitShortLiveCode) //this POST API RQ needed a custom header so It was seperated from the current fitBit redirect page

{
  const authorization_code = 'authorization_code'
  const headersFB = new HttpHeaders() 
  .set('Content-Type', 'application/x-www-form-urlencoded')    //settings as per FITBit guidelines
  .set('Authorization', 'Basic '+'MjJCREtKOjUyZTRjYmQ1MjhjYjgyM2RhZDY1ZTg4OGMxZGJhMDUx');   //client id and client secret concatenated togather and encoded to base 64
  
let FBparams = new HttpParams()
    .set('grant_type', authorization_code)
    .set('client_id', '22BDKJ')
    .set('redirect_uri', 'http://localhost:4200/fitbitRedirect')
    .set("code", FitbitShortLiveCode);

  console.log('fitbitauth ---', FBparams)
  return this.http.post('https://api.fitbit.com/oauth2/token',FBparams, { headers: headersFB })
}


getAccessTokenFromFitbit()
{
  if (this.currentUser.fitbitRefreshToken != null){     //do not call this unless the strava token is in place



  let currentUserDataStr = localStorage.getItem("currentUser"); 
console.log('currentUserDataStr', currentUserDataStr)
  let currentUserData = JSON.parse(currentUserDataStr);
  console.log('currentUserData', currentUserData)
  const storedItems = JSON.parse(localStorage.getItem('currentUser'));
  console.log('storedItems', storedItems)


  
  const headersFB = new HttpHeaders() 
  .set('Content-Type', 'application/x-www-form-urlencoded')    //settings as per FITBit guidelines
  .set('Authorization', 'Basic '+'MjJCREtKOjUyZTRjYmQ1MjhjYjgyM2RhZDY1ZTg4OGMxZGJhMDUx');   //client id and client secret concatenated togather and encoded to base 64
  
let FBparams = new HttpParams()
    .set('grant_type', 'refresh_token')
    .set('refresh_token', storedItems.fitbitRefreshToken);

  return this.http.post(`https://api.fitbit.com/oauth2/token`,FBparams, { headers: headersFB }).subscribe(

     (response) => {


      
      console.log(response)

       //here we are adding the new access token to the local storage
       //split from the currentUser local storage item due to issues surrounding the double save against strava token  
       localStorage.setItem('currentUserfitbitAccessToken',response['access_token']);
       localStorage.setItem('currentUserfitbitUserID',response['user_id']);

       
       var fitbitTokenToDB: any = new FormData()  //creating a new object to send the refresh token to the GatewayAPI call - registerStrava. This will insert the token into the DB for the specified user
          fitbitTokenToDB.append("athleteID", this.currentUser.athleteID);
          fitbitTokenToDB.append("refreshToken", response['refresh_token']);
          
        return this.http.post(`http://localhost:63039/api/Register/registerFitbit`,fitbitTokenToDB).subscribe(   //post refresh token to DB
        )
      },                           
    (error) => {
        console.log(error);
    }
);




}

}

  }
