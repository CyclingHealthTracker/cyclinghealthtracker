import { TestBed } from '@angular/core/testing';

import { ExternalAPIcallsService } from './external-apicalls.service';

describe('ExternalAPIcallsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExternalAPIcallsService = TestBed.get(ExternalAPIcallsService);
    expect(service).toBeTruthy();
  });
});
