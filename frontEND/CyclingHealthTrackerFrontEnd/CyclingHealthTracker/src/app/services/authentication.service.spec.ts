import { TestBed } from '@angular/core/testing';

import { AuthenticateUserService } from './AuthenticateUser.service';

describe('AuthenticationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthenticateUserService = TestBed.get(AuthenticateUserService);
    expect(service).toBeTruthy();
  });
});
