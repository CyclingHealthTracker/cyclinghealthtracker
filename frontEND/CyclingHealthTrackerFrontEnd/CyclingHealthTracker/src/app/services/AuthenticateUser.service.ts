import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';


@Injectable({ providedIn: 'root' })
export class AuthenticateUserService{
    private currUser: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(private http: HttpClient) {


        this.currUser = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currUser.asObservable();
    }

    logoutUser() {
        // remove user from local storage and set current user to null
        //on logout
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentUserfitbitAccessToken');
        localStorage.removeItem('currentUserfitbitUserID');

        this.currUser.next(null);
    }

    public get currentUserValue(): User {
        return this.currUser.value;
    }

    loginUser(username, password) {
        return this.http.post<any>('http://localhost:63039/api/AuthenticteUser', { username, password })
        .pipe(map(userData => {
                localStorage.setItem('currentUser', JSON.stringify(userData));
                this.currUser.next(userData);
                return userData;
            }));
    }


}