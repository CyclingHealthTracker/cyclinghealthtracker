import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Activity } from '../models/activity';
import { Sleep } from '../models/sleep';
import { events } from '../models/cal-event';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserRoles } from '../models/user-roles';
import { Athlete } from '../models/athlete';
import { Coaching } from '../models/coaching';

@Injectable({ providedIn: 'root' })
export class UserAPIService {
    constructor(private http: HttpClient) { }

   loginUser(username: string, password: string){

    return this.http.post<any>('http://localhost:63039/api/AuthenticteUser', { username, password });

   }

    postPasswordRest(user: any)
    {
        return this.http.post(`http://localhost:63039/api/ProfileHandler/ResetPassword?`, user);
    
    }  




    postAthleteUpdate(user: Athlete)
    {
        return this.http.post(`http://localhost:63039/api/ProfileHandler/updateAthlete?`, user);
    
    }  


postUserRoleUpdate(user: UserRoles)
{
    return this.http.post(`http://localhost:63039/api/ProfileHandler?`, user);

}    

getActivitiesForGraph(userName: string)
{
return this.http.get<Activity[]>(`http://localhost:63039/api/ActivityDetails?username=${userName}`).pipe(map(result => result));
}

//return the athlete details for the profile viewer
public getCurrentAthletesDetails(userName: string): Observable<Athlete[]>
{

    return this.http.get<Athlete[]>(`http://localhost:63039/api/AthleteHandler?username=${userName}`).pipe(tap(data => data))


}

//service to retrieve the shchedules data for athlete, used within the athlete and coach flows
public getScheduleData(athleteUserName: string): Observable<events[]>
{    //loading calender 

  return this.http.get<events[]>(`http://localhost:63039/api/ScheduleDetails?athleteUserName=${athleteUserName}`); //return events

}

addSchedule(event: events) {
    return this.http.post(`http://localhost:63039/api/ScheduleDetails?`, event);
}

addCoaching(coachingData: Coaching) {
    return this.http.post(`http://localhost:63039/api/ScheduleDetails?`, coachingData);
}


deleteScheduleByID(id: number){
    return this.http.delete(`http://localhost:63039/api/ScheduleDetails?id=${id}`);
       }

getSleepForGraph(userName: string) {

    return this.http.get<Sleep[]>(`http://localhost:63039/api/SleepDetailsStandard?username=${userName}`).pipe(map(SleepResult => SleepResult));

}

getCoaching(userName: string):Observable<events[]> {

    return this.http.get<events[]>(`http://localhost:63039/api/CoachingDetails?username=${userName}`).pipe(map(coachingRes => coachingRes));
}

getCoachingClients(userName: string) {

    return this.http.get(`http://localhost:63039/api/CoachHandler?username=${userName}`).pipe(map(clientRes => clientRes));

}
    getAllActivities() {
        return this.http.get<Activity[]>('http://localhost:63039/api/ActivityDetails?username=dmcfall');
    }


    getAllSleep() {
        return this.http.get<Sleep[]>('http://localhost:63039/api/SleepDetailsStandard?username=dmcfall');
    }

EditSleepByID(id: number){

       return this.http.get<Sleep[]>(`http://localhost:63039/api/SleepDetailsStandard?id=${id}`);
 }


deleteSleepByID(id: number){
return this.http.delete(`http://localhost:63039/api/SleepDetailsStandard?id=${id}`);
   }



   //register service which uses the userRole model to post data to the server
    register(userRole: UserRoles) {
        return this.http.post(`http://localhost:63039/api/Register/register`, userRole);
    }

    delete(activity: number) {
        return this.http.delete(`http://localhost:63039/api/Users/${activity}`);
    }


}



