import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';


import { AppRoutingModule } from './app-routing.module';
import { ApplicationMainComponent } from './ApplicationMain.component';
import { LoginComponent } from './components/login/login.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertComponent } from './components/alert/alert.component';
import { DashBoard } from './components/dashboard/DashBoard.component';
import { JSONwebTokenInterceptor  } from './helpers/JSONwebTokenInterceptor';
import { ErrorInterceptor } from './helpers/error-interceptor';
import { RegisterComponent } from './components/register/register.component';
import { ActivityLoggingComponent } from './components/activity-logging/activity-logging.component';
import { AgGridModule } from 'ag-grid-angular';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { FullCalendarModule } from '@fullcalendar/angular'; // for FullCalendar!
import { NgxGaugeModule } from 'ngx-gauge';
import { RegisterRedirectComponent } from './components/register-redirect/register-redirect.component';
import { FitBitRegisterRedirectComponent } from './components/fit-bit-register-redirect/fit-bit-register-redirect.component';
import { ClientViewComponent } from './components/client-view/client-view.component';
import { CoachingHandlerComponent } from './components/coaching-handler/coaching-handler.component';



@NgModule({
  declarations: [
    ApplicationMainComponent,
    LoginComponent,
    AlertComponent,
    DashBoard,
    RegisterComponent,
    ActivityLoggingComponent,
    ScheduleComponent,
    RegisterRedirectComponent,
    FitBitRegisterRedirectComponent,
    ClientViewComponent,
    CoachingHandlerComponent,

  ],
  imports: [
    BrowserModule,
    AgGridModule.withComponents([]),
    AppRoutingModule,
    ClarityModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    FullCalendarModule, // for FullCalendar!
    BrowserAnimationsModule,
    NgxGaugeModule


  ],
  providers: [

    { provide: HTTP_INTERCEPTORS, useClass: JSONwebTokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

  ],
  bootstrap: [ApplicationMainComponent]
})
export class AppModule { }
