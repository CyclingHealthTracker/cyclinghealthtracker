import { Component, OnInit, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { UserAPIService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { events } from 'src/app/models/cal-event';
import {HttpClient} from "@angular/common/http";
import { FullCalendarComponent } from '@fullcalendar/angular';


@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  @ViewChild('calendar', {static: false}) calendarComponent : FullCalendarComponent;

//variables declared for the schedule component 
  mdOpenScheduleEvent: boolean;
  viewEvent: boolean;
  eventTitle: string;
  eventDate: string;
  currentUser: User;
  selectedWorkOption: string;
  eventDetails: any;
  scheduleResults: any;
  eventWorkStart: any;
  eventWorkEnd: any;
  date: any;


  public coachingData : events[];
  public calendarEvents : events[];    //calendar events array which is filled from the schedule API
  eventID: number;   //event ID which is needed for the delete RQ to server

  public eventsDouble: any[];


//on init for the schedule page
  ngOnInit() {

    this.getCalEventsRq()
    console.log(this.calendarEvents)
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.refetchEvents()

  }
    

  mdCancel(){     //pairning two bools so we can use one method for both models, this will help clean the code.

    this.mdOpenScheduleEvent = false;
    this.viewEvent = false;


  }






getCalEventsRq(){     //function to call calendar events API, easy accessor as we are calling it twice. Once on
                       //init and the other when the refetch is done during addition
                      //get coaching events
this.userService.getCoaching(this.currentUser.username).subscribe(data => {
this.coachingData = data;
return this.coachingData    

}
);

    this.userService.getScheduleData(this.currentUser.username).subscribe(data => {
      this.calendarEvents = data
      this.eventsDouble= [this.coachingData, this.calendarEvents]
    }
    );
  }

//scheduleForm form
//applying the validators the the add schedule form
scheduleForm = this.formBuilder.group({
  WorkingBool: new FormControl('', Validators.required),
  workStart: new FormControl('', Validators.required),
  workEnd: new FormControl('', Validators.required),
  miscNotes: new FormControl('', Validators.required),
  title: new FormControl('', Validators.required),
  });
  
  calendarPlugins = [dayGridPlugin, interactionPlugin]; // important!


  addSchedule()
{


var formData: any = new FormData();   //formdata to pass into the post request


      formData.append("Athlete.Id", this.currentUser.athleteID);
      formData.append("Title", this.scheduleForm.get('title').value);
      formData.append("date", this.date);
      formData.append("workstart", this.scheduleForm.get('workStart').value);
      formData.append("workFinish", this.scheduleForm.get('workEnd').value);
      formData.append("misc", this.scheduleForm.get('miscNotes').value);


      this.userService.addSchedule(formData).subscribe(    //register post request within the userService service module. 

        (response) => {
          console.log(response)
         
       })

       setTimeout(() => {

this.mdOpenScheduleEvent = false;
let calendarApi = this.calendarComponent.getApi();
this.getCalEventsRq()
calendarApi.refetchEvents()


        }, 700);

}

  selectedWorkingHandler (event: any){
    this.selectedWorkOption = event.target.value;

    console.log(this.selectedWorkOption)


    if(this.selectedWorkOption !== 'Yes'){   
      console.log('disabled controls')
      
      this.scheduleForm.controls['workStart'].disable();
      this.scheduleForm.controls['workEnd'].disable();
      
      }
      if (this.selectedWorkOption !== 'No'){     
        
      console.log('enabled controls')
      
      this.scheduleForm.controls['workStart'].enable();
      this.scheduleForm.controls['workEnd'].enable();
      
      }
    }
  


  handleEventClick(arg)
  {  
this.viewEvent = true;  //setting the view event model to true to initiate opening
this.eventDetails = arg.event.extendedProps.misc
this.eventWorkStart = arg.event.extendedProps.workStart
this.eventWorkEnd = arg.event.extendedProps.workFinish
this.eventID = arg.event.id
this.eventTitle = arg.event.title;
this.eventDate =  moment(arg.event.start).format('MM/DD/YYYY'); //here I have used moment to parse the date from full calender


console.log(arg.event)

    console.log(arg.event.start)
    console.log(arg.event.title)
    console.log(this.eventDetails)

  }
  onClickDeleteSchedule()
  {
this.viewEvent = false;    
console.log(this.eventID)
this.userService.deleteScheduleByID(this.eventID).subscribe(response => {
console.log(response)
})  //passing in the event id to the API delete request to be deleted
//delete request sent to the server

setTimeout(() => {

  this.mdOpenScheduleEvent = false;
  let calendarApi = this.calendarComponent.getApi();
  this.getCalEventsRq()
  calendarApi.refetchEvents()
  
  
          }, 400);
  }
handleDateClick(arg) { //handler method for selecting date

this.selectedWorkOption = 'No';
this.mdOpenScheduleEvent = true;
console.log(arg.dateStr);
console.log(arg);
this.date = arg.dateStr;


  }

  constructor(
  private formBuilder: FormBuilder,
  private userService: UserAPIService,   //using the user service so adding it into the constructor
  private authenticationService: AuthenticateUserService,
  private http: HttpClient, //adding http client to allow for API Rqs
  ) {

    this.currentUser = this.authenticationService.currentUserValue;

   }

    








}



