import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { User} from 'src/app/models/user';
import { Activity} from 'src/app/models/activity';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { UserAPIService } from 'src/app/services/user.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import 'moment-duration-format';
import { AllModules  } from '@ag-grid-enterprise/all-modules';
import '@ag-grid-community/client-side-row-model'
import { Sleep } from 'src/app/models/sleep';
import * as Chart from 'chart.js';



@Component({
  selector: 'app-client-view',
  templateUrl: './client-view.component.html',
  styleUrls: ['./client-view.component.scss']
})
export class ClientViewComponent implements OnInit {

  @ViewChild('agGrid', {static: false}) agGrid: AgGridAngular;

  currentUser: User;
  athleteData: any;
  selectedClient: any;

  constructor(  
    private authenticationService: AuthenticateUserService,
    private userService: UserAPIService,
    private http: HttpClient, //adding http client to allow for API Rqs
) {
    this.currentUser = this.authenticationService.currentUserValue;
}

//vars for graph 
chart: any;
storedSleepArray: Sleep[];
storedActivityArray: Activity[];

//row data for grids
rowData: any;
ActivityRowData: any;

//row data initialized 
//sleep data
columnDefs = [
  {headerName: 'Sleep ID', field: 'sleepId', hide: 'true'},
  {headerName: 'Select', field: '',checkboxSelection: true, width: 400 },
  {headerName: 'Sleep Date', field: 'sleepDate',sortable: true, filter: true, resizable: true, sort: "asc", width: 400 },
  {headerName: 'Sleep Start', field: 'sleepStart', width: 400 },
  {headerName: 'Sleep End', field: 'sleepEnd', width: 400 },
  {headerName: 'Sleep Amount', field: 'sleepAmount', width: 400 },
  {headerName: 'Sleep Quality', field: 'sleepQuality', width: 400},
];

//activity data
ActivityColumnDefs = [
  {headerName: 'Activity ID', field: 'activityId', hide: 'true'},
  {headerName: 'Select', field: '',checkboxSelection: true, width: 400 },
  {headerName: 'Activity Date', field: 'activityDate',sortable: true, filter: true, resizable: true, sort: "asc", width: 400 },
  {headerName: 'Activity Type', field: 'activityType', width: 400 },
  {headerName: 'Activity Amount', field: 'activityAmount'},
  {headerName: 'Activity Quality', field: 'quality', width: 400 },
  {headerName: 'Activity Intensity', field: 'intensity'},
];


//sends request for athlete data to be prefilled
selectedClientHandler (username){
  this.selectedClient = username;
  console.log('selection changed', this.selectedClient)
  this.getClientsData(username)
  this.agGrid.api.setRowData(this.agGrid.rowData)


}

  ngOnInit() {

    this.userService.getCoachingClients(this.currentUser.username).subscribe(data => { //get the clients for coaching
      this.athleteData = data;
      console.log('testing row data for agGrid -- ', this.athleteData)
    })    
 

  }





//get client data
//Large Method
getClientsData(athleteUserName){
 
//loading grapha data 
this.userService.getSleepForGraph(athleteUserName).subscribe(res => {
  this.storedSleepArray = res;

  let sortedSleepData = this.storedSleepArray.sort((b, a) => {
          return <any>new Date(b.sleepDate) - <any>new Date(a.sleepDate);
        });
console.log(this.storedSleepArray)


  this.chart = new Chart('canvasSleep', {
    type: 'line',
    data: {
      labels: sortedSleepData.map(x => x.sleepDate),
      datasets: [
        {
          label: 'Sleep Data',
          data: sortedSleepData.map(x => x.sleepAmount),
        }
      ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
  
    }
  });
}
  )

  this.rowData = this.http.get<Sleep[]>(`http://localhost:63039/api/SleepDetailsStandard?username=${athleteUserName}`);
  this.ActivityRowData = this.http.get<Activity[]>(`http://localhost:63039/api/ActivityDetails?username=${athleteUserName}`);
    

  this.userService.getActivitiesForGraph(athleteUserName).subscribe(res => {

    this.storedActivityArray = res;
    console.log(this.storedActivityArray)

      let sortedData = this.storedActivityArray.sort((b, a) => {
          return <any>new Date(b.activityDate) - <any>new Date(a.activityDate);
        });

  let name = 'Angular 7 chartjs';

  this.chart = new Chart('canvas', {
    type: 'line',
    data: {
      labels: sortedData.map(x => x.activityDate),
      datasets: [
        {
          label: 'Activity Data',
          data: sortedData.map(x => x.activityAmount),
        },  
      ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
  
      }
   });
  }
)




};

}//method end





