import { Component, OnInit, ViewChild } from '@angular/core';
import { first } from 'rxjs/operators';
import { User} from 'src/app/models/user';
import { Activity} from 'src/app/models/activity';
import {ExternalAPIcallsService} from 'src/app/services/external-apicalls.service'; 
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { UserAPIService } from 'src/app/services/user.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { StravaActivity } from 'src/app/models/strava-activity';
import * as moment from 'moment';
import 'moment-duration-format';
import { FormGroup, FormControl } from '@angular/forms';
import { AllModules  } from '@ag-grid-enterprise/all-modules';
import { Module, ModuleRegistry  } from '@ag-grid-community/all-modules';
import '@ag-grid-community/client-side-row-model'
import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';

import '@ag-grid-community/all-modules/dist/styles/ag-grid.css';
import '@ag-grid-community/all-modules/dist/styles/ag-theme-alpine.css';

@Component({ templateUrl: 'home.component.html' 




})
export class DashBoard implements OnInit {

    @ViewChild('agGrid', {static: false}) agGrid: AgGridAngular;


    currentUser: User;
    users = [];
    activities: Activity[];
    hideAthDisplay: boolean;
    
    gaugeType = "semi";
    gaugeValue = 28.3;
    gaugeLabel = "Speed";
    gaugeAppendText = "km/hr";


    //testing
    public gridApi;
    public gridColumnApi;
    public columnDefs;
    public defaultColDef;
    public detailCellRendererParams;
     public rowData: any;
     public modules: Module[] = AllModules;
     
  //testing



//uses var checker to confirm it is a coach
//called only when coach is logged in 
    CoachActivityColumns = [

    ]

    coachingDashBoardData: any; //data for the coach dashboard
    FitbitActivityColumnDefs = [

        {headerName: 'Select', field: '',checkboxSelection: true, width: 100 },
        {headerName: 'Date', field: 'dateOfSleep', width: 300, valueFormatter: this.DateFormatter},
        {headerName: 'Duration', field: 'duration', width: 200, valueFormatter: this.TimeFormatter},
        {headerName: 'Sleep Efficiency', field: 'efficiency', width: 300 },
        {headerName: 'Start Time', field: 'startTime', width: 300 , valueFormatter: this.TimeFormatter},
        {headerName: 'End Time', field: 'endTime', width: 300 , valueFormatter: this.TimeFormatter},

    ];



    DateFormatter(params) {
        return moment(params.value).format('MM/DD/YYYY');    //simple function to format the time within the AG grid to Hours and Minutes
      }

    TimeFormatter(params) {
        return moment(params.value).format('HH.mm');    //simple function to format the time within the AG grid to Hours and Minutes
      }

distanceFormatter(params){

return (params.value/1000) //simple function which divides the value back from strava to by 1000 to display KM
}


movingTimeFormatter(params){

return moment.duration(params.value, 'seconds').format('HH.mm') //using the moment library to convert the duration

}


userRoleHandler()  //checking for user role
{

     if(this.currentUser.userRole == 'Coach'){    //checking for user role for ath displays within the home screen
            this.hideAthDisplay = true
            console.log('hidden logic, ture is coach ---', this.hideAthDisplay)
        }
        if(this.currentUser.userRole == 'Athlete'){   //checking for user role
            this.hideAthDisplay = false
            console.log('hidden logic, ture is coach ---', this.hideAthDisplay)
        }

}

//testing

onFirstDataRendered(params) {
  setTimeout(function() {
    params.api.getDisplayedRowAtIndex(1).setExpanded(true);
  }, 0);
}

onGridReady(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;
this.userService.getCoachingClients(this.currentUser.username).subscribe(data => {
    this.rowData = data;

    console.log('testing row data for agGrid -- ', this.rowData)

  });


}

    ActivityColumnDefs = [
        
        {headerName: 'Select', field: '',checkboxSelection: true, width: 100 },
        {headerName: 'Name', field: 'name', width: 300 },
        {headerName: 'Moving Time', field: 'moving_time', width: 300, valueFormatter: this.movingTimeFormatter},
        {headerName: 'Distance', field: 'distance', width: 200, valueFormatter: this.distanceFormatter},      //distance comes from the strava API in meters   
        {headerName: 'Type', field: 'type', width: 300 },



    ];
    FitBitRowData: any;
    ActivityRowData: any;
    storedActivityArray: Object;

    constructor(
        private externalAPIcalls: ExternalAPIcallsService,
        private authenticationService: AuthenticateUserService,
        private userService: UserAPIService,
        private http: HttpClient, //adding http client to allow for API Rqs

    ) {
        this.currentUser = this.authenticationService.currentUserValue;


        this.columnDefs = [
          {
            field: 'username',
            cellRenderer: 'agGroupCellRenderer',
          },
          { field: 'age' },
          { headerName: 'First Name', field: 'userRole.firstName' },
          { headerName: 'Last Name', field: 'userRole.lastName' },
          { headerName: 'Email', field: 'userRole.emailAddress' },
          { field: 'weight' },
          { field: 'height' },
        ];
        this.defaultColDef = { flex: 1 };
        this.detailCellRendererParams = {
          detailGridOptions: {
            columnDefs: [
              { field: 'activityDate', sort: "desc" },
              { field: 'activityType' },
              {
                field: 'activityAmount',
                minWidth: 150,
              },
              {
                field: 'quality',
                //valueFormatter: "x.toLocaleString() + 's'",
              },
              {
                field: 'intensity',
                minWidth: 150,
              },
            ],
            defaultColDef: { flex: 1 },
          },
          getDetailRowData: function(params) {
            params.successCallback(params.data.activities);
          },
          
        };
    }



    ngOnInit() {

      this.userService.getCoachingClients(this.currentUser.username).subscribe(data => {

        this.coachingDashBoardData = data;
        console.log('coaching test data ----', this.coachingDashBoardData)
        
        })


        this.externalAPIcalls.getAccessTokenFromFitbit(); // retrieving access token from FITBIT.
        this.getStravaAccesssToken(); //oninit, we will call this method to produce  new strava refresh token for our API calls to Strava

        this.userRoleHandler() //calling the user checker for home Athlete items 
        setTimeout(() => {
        
        this.FitBitRowData = this.externalAPIcalls.loadFitbit();


        }, 900);

    


        setTimeout(() => {
            
            this.ActivityRowData = this.externalAPIcalls.loadStravaActivities()

            }, 900);

    }


    
        getStravaAccesssToken(){

       if (this.currentUser.stravaRefreshToken != null){     //do not call this unless the strava token is in place
        var stravaAuth: any = new FormData()

        stravaAuth.append("client_id", '37777');
        stravaAuth.append("client_secret", '452594e203b54e8719e20f3d22e8b96d86adf722'); //hardcoded value, can be changed. 
        stravaAuth.append("grant_type", 'refresh_token'); //grant type must be 'refresh token' when requesting the refresh token  
        stravaAuth.append("refresh_token", this.currentUser.stravaRefreshToken);


        return this.http.post(`https://www.strava.com/oauth/token`,stravaAuth).subscribe(

           (response) => {

            var newRefreshToken = response['refresh_token']
 
        //here we are adding the new access token to the local storage
            let currentUserDataStr = localStorage.getItem("currentUser"); 
            let currentUserData = JSON.parse(currentUserDataStr);
            
            currentUserData.stravaAccessToken = response['access_token']
            let newUserDataStr = JSON.stringify(currentUserData)
            localStorage.setItem("currentUser", newUserDataStr); 

           this.currentUser.stravaAccessToken = response['access_token'];
           console.log('strava access token supplied by refresh  -- ', this.currentUser.stravaAccessToken)


           var stravaTokenToDB: any = new FormData()  //creating a new object to send the refresh token to the GatewayAPI call - registerStrava. This will insert the token into the DB for the specified user
           stravaTokenToDB.append("athleteID", this.currentUser.athleteID);
           stravaTokenToDB.append("refreshToken", newRefreshToken);
           
           return this.http.post(`http://localhost:63039/api/Register/registerStrava`,stravaTokenToDB).subscribe(   //post refresh token to DB
           )

        },
        (error) => {

            console.log(error);

        }
    );
      }
        
      }

    


      MdopenAddexternalFitbit: boolean;
      MdopenAddexternalStrava: boolean;


      mdCancelAddStrava(){
this.MdopenAddexternalStrava = false
this.agGrid.api.deselectAll(); //deselect the selected nodes within the AGgrid
this.agGrid.api.deselectAll

      }


      mdCancelAddFitbit(){
        this.MdopenAddexternalFitbit = false
        this.agGrid.api.deselectAll(); //deselect the selected nodes within the AGgrid
        this.agGrid.api.deselectAll
        
              }

                sleepForm = new FormGroup({
                sleepDate: new FormControl(''),
                sleepStart: new FormControl(''),
                sleepEnd: new FormControl(''),
                sleepQuality: new FormControl(''),
                
                });     
              
              
              
              
                mdAddFitbit(){


                  const selectedNodes = this.agGrid.api.getSelectedNodes();
                  const selectedData = selectedNodes.map( node => node.data );
                 console.log(selectedNodes);
                    
              }
    



      

      onSelectionChangedFitbit(event) {

          const selectedNodes = this.agGrid.api.getSelectedNodes();
          const selectedData = selectedNodes.map( node => node.data );


          console.log("selection", selectedNodes)


          this.MdopenAddexternalFitbit = true
      
          this.sleepForm.setValue({
            //time asleep / (total time in bed - time to fall asleep) - sleep effeciency
        
            sleepDate: selectedData.map( node => node.sleepDate),
            sleepQuality: selectedData.map( node => node.sleepQuality ),
            sleepStart: selectedData.map( node => node.sleepStart ),
            sleepEnd: selectedData.map( node => node.sleepEnd )
            
    
          })

}

             onSelectionChangedStrava(event) {
        
                const selectedNodes = this.agGrid.api.getSelectedNodes();
                const selectedData = selectedNodes.map( node => node.data );
              
                  console.log(selectedNodes);

              
            
                
                  
                     }
          
      
      
      
      }
    

