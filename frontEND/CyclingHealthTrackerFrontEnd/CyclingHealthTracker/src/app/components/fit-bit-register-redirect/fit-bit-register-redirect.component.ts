import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { User } from 'src/app/models/user';
import {ExternalAPIcallsService} from 'src/app/services/external-apicalls.service'; 



@Component({
  selector: 'app-fit-bit-register-redirect',
  templateUrl: './fit-bit-register-redirect.component.html',
  styleUrls: ['./fit-bit-register-redirect.component.scss']
})
export class FitBitRegisterRedirectComponent implements OnInit {

  refreshToken: any;
  currentUser: User;
  constructor(private activatedRoute: ActivatedRoute,
              private http: HttpClient, //adding http client to allow for API Rqs
              private router: Router,
              private authenticationService: AuthenticateUserService,   //auth service to grab the current user
              private externalAPIcalls: ExternalAPIcallsService,

              ) 

{
  this.currentUser = this.authenticationService.currentUserValue;
}



  //onInit when loading a new page

  //eample URL - http://localhost:4200/fitbitRedirect?code=2380bde4d40e0445ffa595d90d5bc39e3ea3ec1f#_=_
  ngOnInit() {   //when registration completes, the user is redirected to the redirect page where the strava short lived code will be grabbed from the url 


    this.activatedRoute.queryParams.subscribe(params => {    //using rhe activated route query params
        const FitbitShortLiveCode = params['code'];         //grabbing the code from the url

        console.log('fibit short live  -- ', FitbitShortLiveCode); //used for debugging purposes
    
this.externalAPIcalls.getRefreshFromFitbit(FitbitShortLiveCode).subscribe(    
          (response) => {                  //this reponse will hold the details of the refresh token for the user 
             console.log(response)        //Access tokens expire six hours after they are created
                                        //we will store the refresh token in our DB to allow us to call it in further instances
          
          this.refreshToken = response['refresh_token'];           
          console.log('refresh token = ', this.refreshToken) //debugging purposes/ testing

          var fitbitTokenToDB: any = new FormData()  //creating a new object to send the refresh token to the GatewayAPI call - registerStrava. This will insert the token into the DB for the specified user
          fitbitTokenToDB.append("athleteID", this.currentUser.athleteID);
          fitbitTokenToDB.append("refreshToken", this.refreshToken);
          
        return this.http.post(`http://localhost:63039/api/Register/registerFitbit`,fitbitTokenToDB).subscribe(   //post refresh token to DB
         )
            },                           
          (error) => {
              console.log(error);
          }
      );
  })

  
  setTimeout(() => {
    
    this.router.navigate(['/login']);

    }, 1300);

  }

}
