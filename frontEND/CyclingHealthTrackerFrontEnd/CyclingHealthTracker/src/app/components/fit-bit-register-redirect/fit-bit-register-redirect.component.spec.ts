import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FitBitRegisterRedirectComponent } from './fit-bit-register-redirect.component';

describe('FitBitRegisterRedirectComponent', () => {
  let component: FitBitRegisterRedirectComponent;
  let fixture: ComponentFixture<FitBitRegisterRedirectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FitBitRegisterRedirectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FitBitRegisterRedirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
