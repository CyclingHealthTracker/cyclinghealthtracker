import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { AlertService } from 'src/app/services/alert.service';
import { ApplicationMainComponent } from 'src/app/ApplicationMain.component';

@Component({ templateUrl: 'login.component.html' })
export class LoginComponent implements OnInit {
    userLoginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticateUserService,
        private alertService: AlertService,
        private ApplicationMainComponent: ApplicationMainComponent
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {


        //formBuilder group
        this.userLoginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
// || if query parms is not available, use /
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

//get the login form data
   get formFields() { return this.userLoginForm.controls; }

   onSubmitLogin() {
        this.submitted = true;

        this.alertService.clear();
        // stop here if form is invalid
        if (this.userLoginForm.invalid) {
           return;
        }

      this.loading = true;
        this.authenticationService.loginUser(this.formFields.username.value, this.formFields.password.value)
           .pipe(first())
            .subscribe(
                data => {
                    console.log('login data --- ', data)   //logging data from the login to console

                    this.router.navigate([this.returnUrl]) //route the user to the returnURL

                    this.ApplicationMainComponent.ngOnInit()   //loading in correct values onInit within the app component 
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false; //throw error to the alert service

                },
            
            );
    }
}
