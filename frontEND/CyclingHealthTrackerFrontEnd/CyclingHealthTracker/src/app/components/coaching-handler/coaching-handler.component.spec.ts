import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoachingHandlerComponent } from './coaching-handler.component';

describe('CoachingHandlerComponent', () => {
  let component: CoachingHandlerComponent;
  let fixture: ComponentFixture<CoachingHandlerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoachingHandlerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoachingHandlerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
