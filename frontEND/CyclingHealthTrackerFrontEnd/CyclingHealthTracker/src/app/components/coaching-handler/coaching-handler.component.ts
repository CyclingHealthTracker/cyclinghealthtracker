import { Component, OnInit, ViewChild } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import { FormControl, Validators, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { UserAPIService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { events } from 'src/app/models/cal-event';
import {HttpClient} from "@angular/common/http";
import { FullCalendarComponent } from '@fullcalendar/angular';


@Component({
  selector: 'app-coaching-handler',
  templateUrl: './coaching-handler.component.html',
  styleUrls: ['./coaching-handler.component.scss']
})
export class CoachingHandlerComponent implements OnInit {


  @ViewChild('calendar', {static: false}) calendarComponent : FullCalendarComponent;

  mdOpenScheduleEvent: boolean;
  viewEvent: boolean;
  eventTitle: string;
  eventDate: string;
  currentUser: User;
  selectedWorkOption: string;
  eventDetails: any;
  scheduleResults: any;
  eventWorkStart: any;
  eventWorkEnd: any;
  date: any;
  public selectedClientData : events[];
  public calendarEvents : events[];    //calendar events array which is filled from the schedule API
  eventID: number;   //event ID which is needed for the delete RQ to server
  public eventsDouble: any[];
  athleteData: any;
  selectedClient: any;





  constructor(
    private formBuilder: FormBuilder,
    private userService: UserAPIService,   //using the user service so adding it into the constructor
    private authenticationService: AuthenticateUserService,
    private http: HttpClient, //adding http client to allow for API Rqs
    ) {
    
    this.currentUser = this.authenticationService.currentUserValue;
    
    }
    
//sends request for athlete data to be prefilled
selectedClientHandler (username){



setTimeout(() => {

  this.selectedClient = username;
  console.log('selection changed', this.selectedClient)
  this.getCalEventsRq()


  
  
  }, 700);

}



    
  ngOnInit() {

     //in place to feth the coaches clients on initialize of the page, this will load the athlete data variable for setting on HTML
     this.userService.getCoachingClients(this.currentUser.username).subscribe(data => {
      this.athleteData = data;
      console.log('testing row data for agGrid -- ', this.athleteData)
      })   

    this.getCalEventsRq()
    console.log(this.calendarEvents)
    let calendarApi = this.calendarComponent.getApi();
    calendarApi.refetchEvents()
  }

  mdCancel(){     //pairning two bools so we can use one method for both models, this will help clean the code.

    this.mdOpenScheduleEvent = false;
    this.viewEvent = false;


  }

  getCalEventsRq(){     //function to call calendar events API, easy accessor as we are calling it twice. Once on
    //init and the other when the refetch is done during addition
   //get coaching events
this.userService.getCoaching(this.selectedClient).subscribe(data => {   //getting data for selected user of coach
this.selectedClientData = data;
console.log('coaching data -- ',this.selectedClientData)
return this.selectedClientData  


}
);
this.userService.getScheduleData(this.selectedClient).subscribe(data => {

this.calendarEvents = data
this.eventsDouble= [this.selectedClientData, this.calendarEvents]

console.log('events data -- ',this.eventsDouble)
}
);

}

//scheduleForm form
//applying the validators the the add schedule form
PrescrobeCoachingForm = this.formBuilder.group({

WorkingBool: new FormControl('', Validators.required),
workStart: new FormControl('', Validators.required),
workEnd: new FormControl('', Validators.required),
miscNotes: new FormControl('', Validators.required),
title: new FormControl('', Validators.required),

});

calendarPlugins = [dayGridPlugin, interactionPlugin]; // important!


addCoaching()
{
var formData: any = new FormData();   //formdata to pass into the post request
formData.append("Athlete.Id", this.selectedClient); // sending post request for selected client
formData.append("Title", this.PrescrobeCoachingForm.get('title').value);
formData.append("date", this.date);
formData.append("workstart", this.PrescrobeCoachingForm.get('workStart').value);
formData.append("workFinish", this.PrescrobeCoachingForm.get('workEnd').value);
formData.append("misc", this.PrescrobeCoachingForm.get('miscNotes').value);


this.userService.addCoaching(formData).subscribe(    //register post request within the userService service module. 

(response) => {
console.log(response)

})

setTimeout(() => {

this.mdOpenScheduleEvent = false;
let calendarApi = this.calendarComponent.getApi();
this.getCalEventsRq()
calendarApi.refetchEvents()


}, 700);

}

handleEventClick(arg)
{  
this.viewEvent = true;  //setting the view event model to true to initiate opening

this.eventDetails = arg.event.extendedProps.misc
this.eventWorkStart = arg.event.extendedProps.workStart
this.eventWorkEnd = arg.event.extendedProps.workFinish
this.eventID = arg.event.id
this.eventTitle = arg.event.title;
this.eventDate =  moment(arg.event.start).format('MM/DD/YYYY'); //here I have used moment to parse the date from full calender


console.log(arg.event)

console.log(arg.event.start)
console.log(arg.event.title)
console.log(this.eventDetails)

}
onClickDeleteSchedule()
{
this.viewEvent = false;    
console.log(this.eventID)
this.userService.deleteScheduleByID(this.eventID).subscribe(response => {
console.log(response)
})  //passing in the event id to the API delete request to be deleted
//delete request sent to the server

setTimeout(() => {

this.mdOpenScheduleEvent = false;
let calendarApi = this.calendarComponent.getApi();
this.getCalEventsRq()
calendarApi.refetchEvents()


}, 400);
}
handleDateClick(arg) { //handler method for selecting date

this.selectedWorkOption = 'No';
this.mdOpenScheduleEvent = true;
console.log(arg.dateStr);
console.log(arg);
this.date = arg.dateStr;


}












}
