import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserAPIService } from 'src/app/services/user.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';



@Component({ templateUrl: 'register.component.html' })

export class RegisterComponent implements OnInit {

selectedUserTypeOption: any = 'Coach';   //var to store the selected user type on the register form

loading = false; //handles duplicate requests going to the post Req while the server is processing and the front end is waiting
mdOpenPrivStatement: boolean;



//activity entry form
Registerform = this.formBuilder.group({

  Weight: new FormControl('',Validators.required),
  Height: new FormControl('',Validators.required),
  CoachEmail: new FormControl('', Validators.required),
  FirstName: new FormControl('', Validators.required),
  LastName: new FormControl('', Validators.required),
  Role: new FormControl('', Validators.required),
  userName: new FormControl('', Validators.required),
  password: new FormControl('', Validators.required),
  emailAddress: new FormControl('', Validators.required),
  age: new FormControl('', Validators.required),
  confirmPassword: new FormControl('',Validators.required),

  });
  errorMessage: any;
  SuccessResponse: any;
  constructor(
    private router: Router,
    private userService: UserAPIService, //adding userService into constructor
    private formBuilder: FormBuilder, //adding formBuilder into constructor
    private http: HttpClient, //adding http client to allow for API Rqs

  ) {
   }

  ngOnInit() {

//empty
  }


  selectedUserTypeChangeHandler (event: any){
    this.selectedUserTypeOption = event.target.value;

    console.log('selection changed')



    if(this.selectedUserTypeOption !== 'Athlete'){   //if the selectedUser on the register form is not athlete, we will  the form controls, 
                                                    //this is an extra step above disabling them on the HTML frontEnd. 
console.log('disabled controls')

      this.Registerform.controls['Weight'].disable();
      this.Registerform.controls['Height'].disable();
      this.Registerform.controls['CoachEmail'].disable();
      this.Registerform.controls['age'].disable();

    }

    if (this.selectedUserTypeOption !== 'Coach'){     //Dynamically enabling them if the user changes selection to Athlete. 

      console.log('enabled controls')
      
            this.Registerform.controls['Weight'].enable();
            this.Registerform.controls['Height'].enable();
            this.Registerform.controls['CoachEmail'].enable();
            this.Registerform.controls['age'].enable();
  
          }

    }

//privacy statement modal - open when boolean is set to true. Boolean is set within method
openPrivModal()
{
this.mdOpenPrivStatement = true;

}
privClose() //close the privacy statement modal on click from user
{
  this.mdOpenPrivStatement = false;
  
  }


    mdRegRq(){

      this.loading = true;

      var formData: any = new FormData();   //formdata to pass into the post request

      formData.append("UserName", this.Registerform.get('userName').value);
      formData.append("Password", this.Registerform.get('password').value);
      formData.append("ConfirmPassword", this.Registerform.get('confirmPassword').value);
      formData.append("EmailAddress", this.Registerform.get('emailAddress').value);
      formData.append("Role", this.Registerform.get('Role').value);
      formData.append("FirstName", this.Registerform.get('FirstName').value);
      formData.append("LastName", this.Registerform.get('LastName').value);
      formData.append("CoachEmail", this.Registerform.get('CoachEmail').value);
      formData.append("Age", this.Registerform.get('age').value);
      formData.append("Weight", this.Registerform.get('Weight').value);
      formData.append("Height", this.Registerform.get('Height').value);

      
  this.userService.register(formData).subscribe(    //register post request within the userService service module. 

    (response) => {
      console.log(response['value'])
       this.errorMessage = null;
       this.SuccessResponse = response['value']; 
       this.Registerform.reset()

       setTimeout(() => {
       
        this.redirectToLogin()

        }, 500);
   },
 

  (error) => {


      this.errorMessage = error; 
      //this.alertService.error(error);
      console.log(error);

  }
);
    }


    
         redirectToLogin() {
        this.router.navigate(['/login']);
    }

    
}

