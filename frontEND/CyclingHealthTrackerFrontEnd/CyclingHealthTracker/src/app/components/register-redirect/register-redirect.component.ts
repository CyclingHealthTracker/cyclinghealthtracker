import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { User } from 'src/app/models/user';

//the strava token storage is orchestrated on this TS
//when developing, it was agreed that we would use on redirect for both FITBIT and Strava but in the end we had to split them up
//this was due to the complexity of the return URL`s returned by each of the services. 


@Component({
  selector: 'app-register-redirect',
  templateUrl: './register-redirect.component.html',
  styleUrls: ['./register-redirect.component.scss']
})
export class RegisterRedirectComponent implements OnInit {
  refreshToken: any;
  currentUser: User;
  constructor(private activatedRoute: ActivatedRoute,
              private http: HttpClient, //adding http client to allow for API Rqs
              private router: Router,
              private authenticationService: AuthenticateUserService,
              ) 
{
  this.currentUser = this.authenticationService.currentUserValue;
}
  //onInit when loading a new page
  ngOnInit() {   //when registration completes, the user is redirected to the redirect page where the strava short lived code will be grabbed from the url 
    this.activatedRoute.queryParams.subscribe(params => {    //using rhe activated route query params
        const stravaShortLiveCode = params['code'];         //grabbing the code from the url

        console.log('strava short live  -- ', stravaShortLiveCode); //used for debugging purposes

        var stravaAuth: any = new FormData()
        stravaAuth.append("client_id", '37777');
        stravaAuth.append("client_secret", '452594e203b54e8719e20f3d22e8b96d86adf722');
        stravaAuth.append("code", stravaShortLiveCode);
        stravaAuth.append("grant_type", 'authorization_code')
        return this.http.post(`https://www.strava.com/api/v3/oauth/token`,stravaAuth).subscribe(
          (response) => {                  //this reponse will hold the details of the refresh token for the user 
             console.log(response)        //Access tokens expire six hours after they are created
                                        //we will store the refresh token in our DB to allow us to call it in further instances
          
          this.refreshToken = response['refresh_token']; 
          
          console.log('refresh token = ', this.refreshToken) //debugging purposes/ testing


          var stravaTokenToDB: any = new FormData()  //creating a new object to send the refresh token to the GatewayAPI call - registerStrava. This will insert the token into the DB for the specified user
          stravaTokenToDB.append("athleteID", this.currentUser.athleteID);
          stravaTokenToDB.append("refreshToken", this.refreshToken);
          
          return this.http.post(`http://localhost:63039/api/Register/registerStrava`,stravaTokenToDB).subscribe(   //post refresh token to DB
          )
            },                           
          (error) => {
              console.log(error);
          }
      );
  })

  
  setTimeout(() => {
    
    this.router.navigate(['/login']);


    }, 1300);



  }

}
