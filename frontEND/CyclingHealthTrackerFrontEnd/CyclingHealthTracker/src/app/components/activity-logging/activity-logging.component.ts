import { Component, OnInit, ViewChild, } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgModel } from '@angular/forms';
import { first, delay, tap, catchError, isEmpty } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { Activity } from 'src/app/models/activity';
import { UserAPIService } from 'src/app/services/user.service';
import { AuthenticateUserService } from 'src/app/services/AuthenticateUser.service';
import { Sleep } from 'src/app/models/sleep';
import { Summary } from 'src/app/models/summary';
import { HttpClient } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { Chart } from 'chart.js';
import { AlertService } from 'src/app/services/alert.service';



@Component({
  selector: 'app-activity-logging',
  templateUrl: './activity-logging.component.html',
  styleUrls: ['./activity-logging.component.scss']
})
export class ActivityLoggingComponent implements OnInit{

@ViewChild('agGrid', {static: false}) agGrid: AgGridAngular;


    
title = 'app';
currentUser: User;
users = [];
activities: Activity[];
sleeps: Sleep[];
sleepDate: any = "";
sleepStart: any;
sleepEnd: any;
sleepDuration: any;
selectedQuality: any;
sleepID: number = 0;
activityActive: boolean = false;
sleepActive: boolean = false;
mdOpenAddEntity
mdOpen: boolean = false;
mdOpenEdit: boolean = false;
mdOpenAddEntityActivity: boolean;
SelectedActivityType: string = '';
quality: any;
intensity: any;
AddActerror: boolean = false;
activityID: any;

//activity data
ActivityColumnDefs = [
    {headerName: 'Activity ID', field: 'activityId', hide: 'true'},
    {headerName: 'Select', field: '',checkboxSelection: true, width: 400 },
      {headerName: 'Activity Date', field: 'activityDate',sortable: true, filter: true, resizable: true, sort: "asc", width: 400 },
    {headerName: 'Activity Type', field: 'activityType', width: 400 },
    {headerName: 'Activity Amount', field: 'activityAmount'},
    {headerName: 'Activity Quality', field: 'quality', width: 400 },
    {headerName: 'Activity Intensity', field: 'intensity'},
];
ActivityRowData: any;

//sleep data
  columnDefs = [
      {headerName: 'Sleep ID', field: 'sleepId', hide: 'true'},
      {headerName: 'Select', field: '',checkboxSelection: true, width: 400 },
      {headerName: 'Sleep Date', field: 'sleepDate',sortable: true, filter: true, resizable: true, sort: "asc", width: 400 },
      {headerName: 'Sleep Start', field: 'sleepStart', width: 400 },
      {headerName: 'Sleep End', field: 'sleepEnd', width: 400 },
      {headerName: 'Sleep Amount', field: 'sleepAmount', width: 400 },
      {headerName: 'Sleep Quality', field: 'sleepQuality', width: 400},
  ];
  rowData: any;

  //iew chcilds needed for the wizardMD components
  @ViewChild("wizardmd", {static: false}) Component
  @ViewChild("wizardmdEdit", {static: false}) Component1
  @ViewChild("wizardmdAdd", {static: false}) Component2
  @ViewChild("wizardmdAddAct", {static: false}) Component3
    
  
  //initiating variables
  
mdOpenEditActivity: boolean;
chart: any;
storedSleepArray: Sleep[];
storedActivityArray: Activity[];
response: any;

errorMessage: string = null;
SuccessResponse: string = null;
  activityDate: any;
  activityAmount: any[];
  activityType: any[];
  sleepAmount: any[];
  sleepQuality: any[];



  constructor(
      private alertService: AlertService,
      private formBuilder: FormBuilder,
      private authenticationService: AuthenticateUserService,
      private userService: UserAPIService,
      private http: HttpClient,
  ) 
  {
      this.currentUser = this.authenticationService.currentUserValue;
  }


//sleep entry form
sleepEntryForm = this.formBuilder.group({
    sleepDate: new FormControl('', Validators.required),
    sleepStart: new FormControl('', Validators.required),
    sleepEnd: new FormControl('', Validators.required),
    sleepQuality: new FormControl('', Validators.required),
    });




//sleep Edit form
sleepForm = new FormGroup({
  sleepStart: new FormControl(''),
  sleepEnd: new FormControl(''),
  sleepQuality: new FormControl(''),
  
  });

//activity edit form
activityForm = new FormGroup({
    sleepStart: new FormControl(''),
    sleepEnd: new FormControl(''),
    sleepQuality: new FormControl(''),
    
    });

//activity entry form
ActivityEntryForm = this.formBuilder.group({
    activityDate: new FormControl('', Validators.required),
    activityType: new FormControl('', Validators.required),
    activityAmount: new FormControl('', Validators.required),
    quality: new FormControl('', Validators.required),
    intensity: new FormControl('',Validators.required),
 
    });

//edit modal functions

mdOP() {
this.mdOpen = true;
}
mdCancel(){
this.mdOpenEdit = false;
this.agGrid.api.deselectAll();
}
mdEdit(){
//post request to edit the data from the modal 
this.mdOpenEdit = false;
this.agGrid.api.deselectAll();
}
 mdOPedit() {
this.mdOpenEdit = true;
this.agGrid.api.deselectAll();
}


//add entity 
//bool value is checked
mdOPAdd() {
   if (this.activityActive) {   //if activity tab is active 
    this.errorMessage = "";
    this.mdOpenAddEntityActivity = true;
    this.mdOpenAddEntity = false;
   }
   if (this.sleepActive) {         //if sleep tab is active 
    this.mdOpenAddEntity = true;
    this.mdOpenAddEntityActivity = false;
   }
}

mdCancelAdd(){
this.mdOpenAddEntity = false;
}

selectedChangeHandler (event: any){
this.selectedQuality = event.target.value;
}



//activity select drop down handlers
onLinkClick(event: any){
console.log('tab change')
this.ngOnInit()
}
selectedTypeChangeHandler(event: any){
    this.SelectedActivityType = event.target.value;
    }
selectedQualityChangeHandler(event: any){
    this.quality = event.target.value;
    }
selectedIntensityChangeHandler(event: any){
    this.intensity = event.target.value;
    }


//add sleep request - using post request to post the sleep entry to API and GEt request to fetch updated data from API 
mdAddRq(){
var formData: any = new FormData();
var timestart = this.sleepEntryForm.get('sleepStart').value 
var timeEnd = this.sleepEntryForm.get('sleepEnd').value


//appending sleep sleep data for the post request to server
formData.append("sleepDate", this.sleepEntryForm.get('sleepDate').value);
formData.append("SleepStart", this.sleepEntryForm.get('sleepStart').value);
formData.append("SleepEnd", this.sleepEntryForm.get('sleepEnd').value);
formData.append("SleepQuality", this.selectedQuality);
formData.append("Athlete.Id",this.currentUser.athleteID);

this.http.post('http://localhost:63039/api/SleepDetailsStandard',formData).subscribe(

  (response) => {
     console.log(response['value'])
      this.errorMessage = null;
      this.SuccessResponse = response['value']; 
      
  },
  (error) => {

    this.SuccessResponse = null;
      this.errorMessage = error; 
      //this.alertService.error(error);
      console.log(error);

  }
);
}

//end of add sleep request






refreshAfterClickSleep(){ //attached to the ok button on the sleep add menu modal

  setTimeout(() => {
    this.rowData = this.http.get<Sleep[]>(`http://localhost:63039/api/SleepDetailsStandard?username=${this.currentUser.username}`);   //get request to get the sleep details for the rows
    this.agGrid.api.setRowData(this.agGrid.rowData)
    this.sleepEntryForm.reset()
    this.mdOpenAddEntity = false;
    this.SuccessResponse = null;
    this.ngOnInit()
}, 500); //action is delayed to allow for DB update from post request to complete

}


refreshAfterClick() //attached to the ok button on the activity add menu modal
{    //timeout has been set for performance issues
    setTimeout(() => {
        this.ActivityRowData = this.http.get<Activity[]>(`http://localhost:63039/api/ActivityDetails?username=${this.currentUser.username}`);
        this.ngOnInit()
        this.agGrid.api.setRowData(this.agGrid.rowData)
            this.ActivityEntryForm.reset()
            this.mdOpenAddEntityActivity = false;
            this.SuccessResponse = null;
        }, 500);
}


mdAddActivityRq(){ //add activity formdata which is collected using append on the entry form.

    var activityformData: any = new FormData();
    activityformData.append("activityDate", this.ActivityEntryForm.get('activityDate').value);
    activityformData.append("ActivityType", this.SelectedActivityType);
    activityformData.append("activityAmount", this.ActivityEntryForm.get('activityAmount').value);
    activityformData.append("quality", this.quality);
    activityformData.append("intensity", this.intensity);

    activityformData.append("Athlete.Id",this.currentUser.athleteID);
    
    this.http.post('http://localhost:63039/api/ActivityDetails',activityformData).subscribe(

        (response) => {
           console.log(response['value'])
            this.errorMessage = null;
            this.SuccessResponse = response['value']; 
            
        },
        (error) => {

          this.SuccessResponse = null;
            this.errorMessage = error; 
            //this.alertService.error(error);
            console.log(error);

        }
    );
}

onClickDeleteActivity() {
  const selectedNodes = this.agGrid.api.getSelectedNodes();
  const selectedData = selectedNodes.map( node => node.data );
  this.activityID = selectedData.map( node => node.id )



  this.http.delete(`http://localhost:63039/api/ActivityDetails?id=${this.activityID}`).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
  )
  setTimeout(() => {
    this.ActivityRowData = this.http.get<Activity[]>(`http://localhost:63039/api/ActivityDetails?username=${this.currentUser.username}`);
    this.agGrid.api.setRowData(this.agGrid.rowData)
    this.mdOpenEditActivity = false;
    this.ngOnInit()
}, 900);
}



onClickDeleteSleep() {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map( node => node.data );
    this.sleepID = parseInt(selectedData.map( node => node.sleepId ).toString()),
    this.http.delete(`http://localhost:63039/api/SleepDetailsStandard?id=${this.sleepID}`).subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
    )
setTimeout(() => {
  this.rowData = this.http.get<Sleep[]>(`http://localhost:63039/api/SleepDetailsStandard?username=${this.currentUser.username}`);
  this.agGrid.api.setRowData(this.agGrid.rowData)
  this.mdOpenEdit = false;
  this.ngOnInit()
}, 900);
}


ngOnInit() {


      this.userService.getSleepForGraph(this.currentUser.username).subscribe(res => {    //collecting data for graphs on init
      this.storedSleepArray = res;
      let sortedSleepData = this.storedSleepArray.sort((b, a) => {    //sorts the dates of the stored sleep array for the graph
              return <any>new Date(b.sleepDate) - <any>new Date(a.sleepDate); 
            });

            //dates are sorted from b -> A to allow greatest date first

console.log(this.storedSleepArray)

       this.chart = new Chart('canvasSleep', {
        type: 'line',
        data: {
          labels: sortedSleepData.map(x => x.sleepDate),
          datasets: [
            {
              label: 'Sleep Data',
              data: sortedSleepData.map(x => x.sleepAmount),
            }
          ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
      
        }
      });
    }
      )


      this.rowData = this.http.get<Sleep[]>(`http://localhost:63039/api/SleepDetailsStandard?username=${this.currentUser.username}`);
      this.ActivityRowData = this.http.get<Activity[]>(`http://localhost:63039/api/ActivityDetails?username=${this.currentUser.username}`);
        

      this.userService.getActivitiesForGraph(this.currentUser.username).subscribe(res => {

        this.storedActivityArray = res;
        console.log(this.storedActivityArray)
    
          let sortedData = this.storedActivityArray.sort((b, a) => {
              return <any>new Date(b.activityDate) - <any>new Date(a.activityDate);
            });

      let name = 'Angular 7 chartjs';

      this.chart = new Chart('canvas', {
        type: 'line',
        data: {
          labels: sortedData.map(x => x.activityDate),
          datasets: [
            {
              label: 'Activity Data',
              data: sortedData.map(x => x.activityAmount),
            },  
          ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
      
          }
       });
      }
    )}




onSelectionChanged(event) {   //event change listener which is attached the AG grid and triggered by clicking on a row
  const selectedNodes = this.agGrid.api.getSelectedNodes(); //using ag grid api to get selected node row dat
  const selectedData = selectedNodes.map( node => node.data );


console.log(selectedData)

    if (this.activityActive) { //if the activity tab is open, the activity data is collected from the nodes
      


      this.activityDate = selectedData.map(node => node.activityDate)
      this.activityType = selectedData.map(node => node.activityType)
      this.activityAmount = selectedData.map(node => node.activityAmount)
      this.quality = selectedData.map(node => node.quality)
      this.intensity = selectedData.map(node => node.intensity)

      this.mdOpenEditActivity = true;
       }
    
       if (this.sleepActive) {    //if sleep tab is open
          

        this.mdOpenEdit = true; //when the sleep tab is open, collect the node data from AG grid


        this.sleepDate = selectedData.map(node => node.sleepDate)
        this.sleepStart = selectedData.map(node => node.sleepStart)
        this.sleepEnd = selectedData.map(node => node.sleepEnd)
        this.sleepAmount = selectedData.map(node => node.sleepAmount)
        this.sleepQuality = selectedData.map(node => node.sleepQuality)
    
       }
    
}

}
