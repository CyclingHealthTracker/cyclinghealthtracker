import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ApplicationMainComponent } from './ApplicationMain.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        ApplicationMainComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(ApplicationMainComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'CyclingHealthTracker'`, () => {
    const fixture = TestBed.createComponent(ApplicationMainComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('CyclingHealthTracker');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(ApplicationMainComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('CyclingHealthTracker app is running!');
  });
});
