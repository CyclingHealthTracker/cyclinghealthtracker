import { Time } from '@angular/common';

export class Fitbit {

    dateOfSleep: string;
    duration: number;
    efficiency: number;
    startTime: Time;
    endTime: Time;

}
