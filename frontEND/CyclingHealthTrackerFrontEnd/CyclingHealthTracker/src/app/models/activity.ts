export class Activity {

        id: number;
        activityDate: Date;
        activityType: string;
        activityAmount: any;
        quality: string;
        intensity: string;
}
