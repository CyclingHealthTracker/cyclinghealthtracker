export class User {
    id: number;
    username: string;
    userRole: string;
    firstName: string;
    lastName: string;
    token: string;
    emailAddress: string;
    athleteID: number;
    stravaAccessToken: string;
    stravaRefreshToken: string;
    fitbitAccessToken: string;
    fitbitRefreshToken: string;
    fitbitUserID: string;

//values only for athlete will be shown below
//handled on API side, returned if user is an athlete
    age: number;
    weight: number;
    height: number;
    coachEmail: string;

}