import { Time } from '@angular/common'

export class events {
    
    title: string
    date: Date 
    workStart: Time
    workFinish: Time

}