import { Time } from '@angular/common';

export interface CalEvents {
    id: number
    title: string
    date: Date 
    workStart: Time
    workFinish: Time
    misc: string
}
