export class Sleep {

    sleepID: number;
    sleepDate: Date;
    sleepStart: string;
    sleepEnd: string;
    sleepAmount : number;
    sleepQuality : number;

}
