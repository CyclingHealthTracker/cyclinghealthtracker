import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { ModuleRegistry, AllCommunityModules } from '@ag-grid-community/all-modules';

if (environment.production) {
  enableProdMode();
}

ModuleRegistry.registerModules(AllCommunityModules);
platformBrowserDynamic().bootstrapModule(AppModule)
 .catch(err => console.error(err));

//AG grid licence key

import {LicenseManager} from "@ag-grid-enterprise/core";
LicenseManager.setLicenseKey("For_Trialing_ag-Grid_Only-Not_For_Real_Development_Or_Production_Projects-Valid_Until-27_June_2020_[v2]_MTU5MzIxMjQwMDAwMA==7a87475406d5369e9c418e4d9f1ee0dd");

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

