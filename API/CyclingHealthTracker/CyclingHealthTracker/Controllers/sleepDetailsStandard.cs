﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Data.SqlClient;

namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class SleepDetailsStandard : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public SleepDetailsStandard(IConfiguration config)
        {
            _config = config;
        }




        [HttpGet]
        public ActionResult<List<Sleep>> Get([FromQuery] string username)
        {

            List<Sleep> sleeps = null;
            _db = new CyclingHealthTrackerContext();
            sleeps = _db.Sleep.Where(b => b.Athlete.Username == username).ToList();


            return Ok(sleeps);
        }


   

 
        [HttpDelete, Authorize]
        public ActionResult<Sleep> Delete(int id)
        {
            _db = new CyclingHealthTrackerContext();

            Sleep slp = _db.Sleep.FirstOrDefault(s => s.sleepId == id);
            _db.Sleep.Remove(slp);
            _db.SaveChanges();
            return Ok("sleep removed");
        }

        [HttpPost]
        public ActionResult<Sleep> Create(Sleep sleep)
        {



            _db = new CyclingHealthTrackerContext();





            Athlete ath = _db.Athletes.FirstOrDefault(s => s.Id == sleep.Athlete.Id);


            var sleepExists = _db.Sleep.Any(x => x.SleepDate == sleep.SleepDate && x.Athlete.Id == ath.Id);

            if (sleepExists)
            {
                return BadRequest("Sleep Date already exists");

            }

     
              //setting the sleep start date
            var startTm = (sleep.SleepStart);
            var startString = string.Concat(sleep.SleepDate," - ",sleep.SleepStart);
            var endString = string.Concat(sleep.SleepDate, " - ", sleep.SleepEnd);
            var parsedStart = DateTime.ParseExact(startString, "MM/dd/yyyy - HH:mm", System.Globalization.CultureInfo.InvariantCulture);
            var parsedEnd = DateTime.ParseExact(endString, "MM/dd/yyyy - HH:mm", System.Globalization.CultureInfo.InvariantCulture);
            var endDT = parsedEnd.AddDays(1);
            TimeSpan amount = parsedStart - endDT;

            var newAmount = amount.TotalMinutes;
            var mewAmountString = amount.ToString(@"hh\.mm");

            //ToString(@"hh\:mm");
            //var sleepAmount = string.Concat(amount.Hours,'.',amount.Minutes);

            var SleepAmountParsed = Convert.ToDouble(mewAmountString);

            if (SleepAmountParsed <= 0)
            {
                return BadRequest("Sleep Amount must be more than 0");

            }

            Sleep newobj = new Sleep()

            {
                Athlete = ath,
                SleepDate = sleep.SleepDate,
                SleepStart = sleep.SleepStart,
                SleepEnd = sleep.SleepEnd,
                SleepQuality = sleep.SleepQuality,
                SleepAmount = SleepAmountParsed
            };


            //ath.Sleep.Add(newobj);


            _db.Sleep.Add(newobj);
            _db.SaveChanges();

            return Ok(Json("Sleep added"));
        }


    }
}