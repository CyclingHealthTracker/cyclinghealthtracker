﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;

namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class RegisterController : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public RegisterController(IConfiguration config)
        {
            _config = config;
        }


        //post request for storing the strava refresh token
        [AllowAnonymous]

        [HttpPost("registerStrava")]
        public ActionResult RegisterStrava(string refreshToken, int athleteID)
        {
            _db = new CyclingHealthTrackerContext();


            Athlete ath = _db.Athletes.FirstOrDefault(b => b.Id == athleteID);
            ath.StravaToken = refreshToken;
            _db.Athletes.Update(ath);
            _db.SaveChanges();

            return Ok(Json("Token successfully added"));



        }

        //post request for storing the fitbit token
        [AllowAnonymous]

        [HttpPost("registerFitbit")]
        public ActionResult RegisterFitbit(string refreshToken, int athleteID)
        {
            _db = new CyclingHealthTrackerContext();


            Athlete ath = _db.Athletes.FirstOrDefault(b => b.Id == athleteID);
            ath.FitbitToken = refreshToken;
            _db.Athletes.Update(ath);
            _db.SaveChanges();

            return Ok(Json("Fitbit Token successfully added"));



        }

        [AllowAnonymous]
        [HttpPost("register")]
        public ActionResult Register(Register obj)
        {
            _db = new CyclingHealthTrackerContext();
            var isEmailAlreadyExists = _db.UserRoles.Any(x => x.EmailAddress == obj.EmailAddress);
            var usernameAlreadyExists = _db.UserRoles.Any(x => x.Username == obj.UserName);
            var coachDoesNotExist = _db.UserRoles.Any(x => x.EmailAddress == obj.CoachEmail && x.Role == "Coach");
            var confirmPasswordCheck = obj.Password != obj.ConfirmPassword;




            if (isEmailAlreadyExists)
            {
                return BadRequest("Email address already exists");
            }
            if (obj.Role == "Athlete" && !coachDoesNotExist)
            {
                return BadRequest("Coach does not exist, please check email and try again");
            }
            if (usernameAlreadyExists)
            {
                return BadRequest("Username already exists");
            }
            if (confirmPasswordCheck)
            {
                return BadRequest("Password and confirm password do not match");
            }

            else
            {

                //creating the login obj first and add it to the DB so that we can handle the foreign key on the user role side.

                Login newLoginObj = new Login();
                newLoginObj.Username = obj.UserName;
                newLoginObj.Password = obj.Password;
                _db.Logins.Add(newLoginObj);
                _db.SaveChanges();

            }
            {
                Login logins = _db.Logins.FirstOrDefault(b => b.Username == obj.UserName); 
                //creates a list of logins

                UserRole newobj = new UserRole();
                newobj.Username = obj.UserName;
                newobj.EmailAddress = obj.EmailAddress;
                newobj.Role = obj.Role;
                newobj.FirstName = obj.FirstName;
                newobj.LastName = obj.LastName;
                newobj.LoginId = logins.LoginId;

                _db.UserRoles.Add(newobj);
                _db.SaveChanges();

                if(obj.Role == "Athlete") //if the user role passed from the front end registration is "Athlete", we will create a new Athlete object and insert it into the DB
                {
                    Coach coachData = _db.Coaches.FirstOrDefault(b => b.UserRole.EmailAddress == obj.CoachEmail);
                    UserRole athUser = _db.UserRoles.FirstOrDefault(b => b.Username == obj.UserName);

                    Athlete newAth = new Athlete();    //creating new athlete object and inserting it into the DB

                    newAth.CoachID = coachData.CoachId;
                    newAth.Age = obj.Age;
                    newAth.Weight = obj.Weight;
                    newAth.Username = obj.UserName;
                    newAth.UserRoleID = athUser.UserRoleId;

                    _db.Athletes.Add(newAth);
                    _db.SaveChanges();
                }
                if (obj.Role == "Coach")
                {

                    UserRole coachUser =  _db.UserRoles.FirstOrDefault(b => b.Username == obj.UserName);
                    Coach newCoach = new Coach(); //if the user role passed from the front end registration is "Coach", we will create a new Coach object and insert it into the DB

                    newCoach.UserName = obj.UserName;
                    newCoach.UserRoleID = coachUser.UserRoleId;

                    _db.Coaches.Add(newCoach);
                    _db.SaveChanges();
                }

                return Ok(Json("User successfully added"));

            }


        }

    }

}

    




    