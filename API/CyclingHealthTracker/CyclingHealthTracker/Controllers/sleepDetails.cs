﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Data.SqlClient;

namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class SleepDetails : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public SleepDetails(IConfiguration config)
        {
            _config = config;
        }




        [HttpGet]
        public ActionResult <List<Sleep>> Get([FromQuery] string username)
        {

            List<Sleep> sleeps = null;
            _db = new CyclingHealthTrackerContext();
            sleeps = _db.Sleep.Where(b => b.Athlete.Username == username).ToList();

            var ctx = new MLContext();

            var dataSet = sleeps.Select(o => new SleepData
            {
                Amount = float.Parse(o.SleepAmount.ToString()),
                Date = o.SleepDate
            });

            var _dataSetSize = dataSet.Count();
            var dataView = ctx.Data.LoadFromEnumerable(dataSet);
            IEnumerable<SleepData> empty = new List<SleepData>();
            var emptyDataView = ctx.Data.LoadFromEnumerable(empty);
            var iidSpikeEstimator = ctx.Transforms.DetectIidSpike(outputColumnName: nameof(SleepPrediction.Prediction), inputColumnName: nameof(SleepData.Amount), confidence: 95, pvalueHistoryLength: _dataSetSize / 4);

            ITransformer iidSpikeTransform = iidSpikeEstimator.Fit(emptyDataView);

            IDataView transformedData = iidSpikeTransform.Transform(dataView);

            var predictions = ctx.Data.CreateEnumerable<SleepPrediction>(transformedData, reuseRowObject: false);

            //Console.WriteLine("Alert\tScore\tP-Value");

            //foreach (var p in predictions)
            //{
            //    var results = $"{p.Prediction[0]}\t{p.Prediction[1]:f2}\t{p.Prediction[2]:F2}";

            //    if (p.Prediction[0] == 1)
            //    {
            //        results += " <-- Spike detected";
            //    }

            //    Console.WriteLine(results);
            //}
            //Console.WriteLine("");

            return Ok(predictions);


        }

        public class SleepData 
        {
            [LoadColumn(0)]
            public string Date { get; set; }
            [LoadColumn(1)]
            public float Amount { get; set; }
        }

        public class SleepPrediction 
        {
            [VectorType(3)]
            public double[] Prediction { get; set; }
        }

    }


}