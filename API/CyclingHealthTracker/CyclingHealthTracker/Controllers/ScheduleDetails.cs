﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;


namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class ScheduleDetails : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public ScheduleDetails(IConfiguration config)
        {
            _config = config;
        }



        [HttpGet, Authorize] //get request to return the schedules of an athlete
        public ActionResult <List<Schedule>> Get([FromQuery] string athleteUserName) //listing athlete schedule based on userName
        {
            List<Schedule> scheduleData = null;


            _db = new CyclingHealthTrackerContext();

            scheduleData = _db.Schedules.Where(b => b.Athlete.Username == athleteUserName).ToList();

            return Json(scheduleData);


        }

        [HttpDelete]
        public ActionResult<Schedule> Delete(int id)
        {
            _db = new CyclingHealthTrackerContext();

            Schedule sch = _db.Schedules.FirstOrDefault(s => s.Id == id);



            _db.Schedules.Remove(sch);
            _db.SaveChanges();


            return Ok("schedule entry removed");
        }


        [HttpPost, Authorize] //must be authorized with token to post data 
        public ActionResult<Activities> Create(Schedule sch)
        {
            _db = new CyclingHealthTrackerContext();
            Athlete athl = _db.Athletes.FirstOrDefault(s => s.Id == sch.Athlete.Id);
            Schedule newobj = new Schedule()
            {
                Athlete = athl,
                Title = sch.Title,
                date = sch.date,
                Misc = sch.Misc,
                WorkStart = sch.WorkStart,
                WorkFinish = sch.WorkFinish,
            };

            _db.Schedules.Add(newobj);
            _db.SaveChanges();
            return Ok(Json("Schedule added"));
        }




    }


}