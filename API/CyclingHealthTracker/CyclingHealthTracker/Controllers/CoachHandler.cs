﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class CoachHandler : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public CoachHandler(IConfiguration config)
        {
            _config = config;

        }

        [HttpGet, Authorize]
        public ActionResult<List<Athlete>> Get([FromQuery] string username)
        {
            List<Athlete> activities = null;
            _db = new CyclingHealthTrackerContext();
            var checkCoach = _db.UserRoles.Any(x => x.Username == username && x.Role == "Coach");
            if (checkCoach)
            {
                activities = _db.Athletes.Where(b => b.Coach.UserName == username).ToList();
                return activities;
            }
            else return BadRequest("Coach does not exist");
        }

    }

}