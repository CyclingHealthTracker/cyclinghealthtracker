﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;


namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class CoachingDetails : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public CoachingDetails(IConfiguration config)
        {
            _config = config;
        }


        //post request


        [HttpGet]
        public ActionResult <List<Coaching>> Get([FromQuery] string username)  //passing in athlete user to retrieve
        {
            List<Coaching> coachingData = null;


            _db = new CyclingHealthTrackerContext();

            coachingData = _db.Coaching.Where(b => b.Athlete.Username == username).ToList();

            return coachingData;

        }

        [HttpPost, Authorize] //must be authorized with token to post data 
        public ActionResult<Coaching> Create(Coaching input)
        {
            _db = new CyclingHealthTrackerContext();

            Athlete athl = _db.Athletes.FirstOrDefault(s => s.Username == input.Athlete.Username); //athlete equals the athlete.username

            Coaching newobj = new Coaching()
            {
                Athlete = athl,
                PrescribedAmount = input.PrescribedAmount,
                Date = input.PrescribedAmount,
                Misc = input.Misc,
                PrescribedEffort=input.PrescribedEffort,
                BorderColor = "red",

            };

            _db.Coaching.Add(newobj);
            _db.SaveChanges();
            return Ok(Json("Coaching added"));
        }


    }


}