﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class ProfileHandler : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public ProfileHandler(IConfiguration config)
        {
            _config = config;

        }

        [HttpGet]
        public IActionResult GetByUsername(string UserName)
        {
            IActionResult response = null;

            //creating a new cyclingHealthTracker DB context to access the DB
            _db = new CyclingHealthTrackerContext();

            //returning the athlete to the front end for further use

           // Athlete athlete = _db.Athletes.FirstOrDefault(b => b.Username == UserName);
           // Coach coach = _db.Coaches.FirstOrDefault(b => b.UserName == UserName);
            UserRole user = _db.UserRoles.FirstOrDefault(b => b.Username == UserName);


            if (user.Role == "Coach")
            {

                response = Ok(new { username = user.Username, firstName = user.FirstName, lastName = user.LastName, userRole = user.Role, emailAddress = user.EmailAddress });


            }
            else
            {
                Athlete athUser = _db.Athletes.FirstOrDefault(b => b.Username == user.Username);
                response = Ok(new { username = user.Username, firstName = user.FirstName, lastName = user.LastName, emailAddress = user.EmailAddress, userRole = user.Role, athleteID = athUser.Id, stravaRefreshToken = athUser.StravaToken, fitbitRefreshToken = athUser.FitbitToken, age = athUser.Age, weight = athUser.Weight, height = athUser.Height, CoachEmail = athUser.Coach.UserRole.EmailAddress });   //the JSON object which is passed back to the browser upon login.

            }
            return response;
                
        }
        [HttpPost, Authorize] //must be authorized with token to post data 
        public ActionResult<UserRole> UpdateUser(UserRole User)  //update user component -- post request will be sent from the UI
        {
            _db = new CyclingHealthTrackerContext();


            UserRole userUpdate = _db.UserRoles.FirstOrDefault(b => b.Username == User.Username);    //checking against the userName


            userUpdate.FirstName = User.FirstName;   //setting the object to be updated into the database
            userUpdate.LastName = User.LastName;
            userUpdate.EmailAddress = User.EmailAddress;



            _db.UserRoles.Update(userUpdate); //saving the update which has been set by the request

            _db.SaveChanges(); //save changes


            return Ok(Json("User Updated"));

        }

        [HttpPost("updateAthlete"), Authorize] //must be authorized with token to post data 
        public ActionResult<Athlete> UpdateAthlete(Athlete UserAth)  //update user component -- post request will be sent from the UI
        {
            _db = new CyclingHealthTrackerContext();


            Athlete AthUpdate = _db.Athletes.FirstOrDefault(b => b.Username == UserAth.Username);    //checking against the userName



            AthUpdate.Age = UserAth.Age;      //setting the values for the athlete update
            AthUpdate.Weight = UserAth.Weight;
            AthUpdate.Height = UserAth.Height;

            _db.Athletes.Update(AthUpdate); //saving the update which has been set by the request

            var ChangesMade = _db.ChangeTracker.HasChanges();


            _db.SaveChanges(); //save changes


            if (ChangesMade)
            {


                return Ok(Json("Athlete Updated"));


            }
            else
            {

                return Ok(Json("No Updates"));

            }

        }




              [HttpPost("ResetPassword"), Authorize] //must be authorized with token to post data 
        public ActionResult<Login> PasswordReset(PasswordReset loginCreds)  //update user component -- post request will be sent from the UI
        {
            _db = new CyclingHealthTrackerContext();


            Login passwordUpdateClient = _db.Logins.FirstOrDefault(b => b.Username == loginCreds.UserName);    //checking against the userName
            var currentPasswordIncorrect = _db.Logins.Any(x => x.Password != loginCreds.CurrentPassword); //checking the current password
            var newPasswordsMisMatch = (loginCreds.NewPassword != loginCreds.ConfirmNewPassword);   //bool to check if the passwords match 
            if (currentPasswordIncorrect)
            {

                return BadRequest("Current Password is incorrect");

            }

            if (newPasswordsMisMatch)
            {


                return BadRequest("New password and confirm new password does not match");

                //we do not use JSON serial here 


            }


            passwordUpdateClient.Password = loginCreds.NewPassword;
            _db.Logins.Update(passwordUpdateClient); //saving the update which has been set by the request


            var ChangesMade = _db.ChangeTracker.HasChanges();
            _db.SaveChanges(); //save changes


                return Ok(Json("Password Updated"));


        }










    }







}