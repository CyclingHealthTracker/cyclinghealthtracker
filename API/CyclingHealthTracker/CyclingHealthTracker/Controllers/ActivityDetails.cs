﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;
using System.Collections.Generic;
using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class ActivityDetails : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;
            
        public ActivityDetails(IConfiguration config)
        {
            _config = config;
            
        }


        //Get request to retrieve activites 
        [HttpGet]
        public ActionResult <List<Activities>> Get([FromQuery] string username)
        {

            List<Activities> activities = null;



            _db = new CyclingHealthTrackerContext();


            var checkCoach =  _db.UserRoles.Any(x => x.Username == username && x.Role == "Coach");


            if (checkCoach)
            {

                activities = _db.Activities.Where(b => b.Athlete.Coach.UserName == username).ToList();


            }
            else
            {

                activities = _db.Activities.Where(b => b.Athlete.Username == username).ToList();

            }

            return activities;

        }
     
        

        [HttpPost, Authorize] //must be authorized with token to post data 
        public ActionResult<Activities> Create(Activities activity)
        {
            _db = new CyclingHealthTrackerContext();

            //if the activity added to the activity Db matches a schedule workout, then change the background and border to green

            DateTime parsedActivity = DateTime.ParseExact(activity.ActivityDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);


            string formattedDate = parsedActivity.ToString("yyyy-MM-dd");

            var prescribedCoachingExists = _db.Coaching.Any(x => x.Date == formattedDate && activity.Athlete.Id == x.Athlete.Id);

            var futureActivity = parsedActivity > DateTime.Now;    //checking if the new activity is greater than todays date

            if (futureActivity)
            {
                return BadRequest("Activity Date cannot be in the future");

            }

            if (prescribedCoachingExists) //if there are any dates within the prescribed coaching which matches the activity date
            {

                Coaching chc = _db.Coaching.FirstOrDefault(x => x.Date == formattedDate && activity.Athlete.Id == x.Athlete.Id);

                chc.BorderColor = "green";
                chc.BackgroundColor = "green";

                _db.Coaching.Update(chc);
                _db.SaveChanges();


            }

            Athlete ath = _db.Athletes.FirstOrDefault(s => s.Id == activity.Athlete.Id); //creates ath of type athlete where the ID matches the ID in DB

            var activityExists = _db.Activities.Any(x => x.ActivityDate == activity.ActivityDate && x.Athlete.Id == ath.Id); //if activity exists Var


            if (activityExists) //if the activity already exists
            {
                return BadRequest("Activity Date already exists");  //sends message to front end

            }

            if (activity.ActivityAmount <= 0) //if activity amount is less that zero
            {
                return BadRequest("Activity Amount must be more than 0"); //sends error response back to client

            }


            Activities newobj = new Activities()
            {
                Athlete = ath,
                ActivityDate = activity.ActivityDate,
                ActivityAmount = activity.ActivityAmount,
                ActivityType = activity.ActivityType,
                Quality = activity.Quality,
                Intensity = activity.Intensity
            };


            _db.Activities.Add(newobj);
            _db.SaveChanges();


            return Ok(Json("Activity added"));
        }
        //delete request to remove activity
        [HttpDelete, Authorize]
        public ActionResult<Activities> Delete(int id)

        {
            _db = new CyclingHealthTrackerContext();

            //get activity details first 


            Activities activity = _db.Activities.FirstOrDefault(b => b.Id == id);

            DateTime parsedActivity = DateTime.ParseExact(activity.ActivityDate, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            string formattedDate = parsedActivity.ToString("yyyy-MM-dd");


            var prescribedCoachingExists = _db.Coaching.Any(x => x.Date == formattedDate);   //here we check the coaching date against the formatted date


            //within this controller we padd in an activity ID to be deleted from the server
            //we will first check if there are any schedules paired with this actvity, if so, we will change the schedule to RED




            if (prescribedCoachingExists) //if there are any dates within the prescribed coaching which matches the activity date
            {

                Coaching chc = _db.Coaching.FirstOrDefault(x => x.Date == formattedDate && x.Athlete.Id == activity.AthleteId);

                chc.BorderColor = "orange";
                chc.BackgroundColor = "orange";

                _db.Coaching.Update(chc);
                _db.SaveChanges();


            }

            _db = new CyclingHealthTrackerContext();


   


            Activities act = _db.Activities.FirstOrDefault(s => s.Id == id);

            _db.Activities.Remove(act);
            _db.SaveChanges();

            return Ok("Activity removed");
        }

    }


}