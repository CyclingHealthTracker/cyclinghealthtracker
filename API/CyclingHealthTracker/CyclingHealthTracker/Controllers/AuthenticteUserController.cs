﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using CyclingHealthTracker.Database;
using CyclingHealthTracker.Models;


namespace CyclingHealthTracker.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticteUserController : Controller
    {
        internal CyclingHealthTrackerContext _db;
        private IConfiguration _config;

        public AuthenticteUserController(IConfiguration config)
        {
            _config = config;
        }



        //post request

        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody]Login login)
        {
            IActionResult response = Unauthorized();
            var user = Authenticate(login);

            if (user != null)
            {

                var tokenString = BuildToken(user);

                //these are the details which are passed back once a user logs in, from the authentication service API
                if (user.Role == "Coach")
                {

                    response = Ok(new { username = user.Username, firstName = user.FirstName, lastName = user.LastName, userRole = user.Role, emailAddress = user.EmailAddress, token = tokenString});


                }
                else
                {
                    Athlete athUser = _db.Athletes.FirstOrDefault(b => b.Username == user.Username);
                    response = Ok(new { username = user.Username, firstName = user.FirstName, lastName = user.LastName, emailAddress = user.EmailAddress, userRole = user.Role, token = tokenString, athleteID = athUser.Id, stravaRefreshToken = athUser.StravaToken, fitbitRefreshToken = athUser.FitbitToken, age = athUser.Age, weight = athUser.Weight, height = athUser.Height, CoachEmail = athUser.Coach.UserRole.EmailAddress});   //the JSON object which is passed back to the browser upon login.

                }


                return response;
            }
            else
            {

              return BadRequest("UserName or Password incorrect");


            }
        }

        private string BuildToken(UserRole user)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(1000000.5), //set how long the Token takes to expire
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }



        private UserRole Authenticate(Login login)
        {
            _db = new CyclingHealthTrackerContext();
            var myUser = _db.Logins
               .FirstOrDefault(u => u.Username == login.Username
               && u.Password == login.Password);
               UserRole user = null;


            if (myUser != null)    //User was found
            {
                user = _db.UserRoles.Where(b => b.Username == login.Username).FirstOrDefault();
            }

            return user;
        }

    }
}