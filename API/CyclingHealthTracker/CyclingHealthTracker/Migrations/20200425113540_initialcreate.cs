﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CyclingHealthTracker.Migrations
{
    public partial class initialcreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Logins",
                columns: table => new
                {
                    LoginId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Username = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logins", x => x.LoginId);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserRoleId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Username = table.Column<string>(nullable: true),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    EmailAddress = table.Column<string>(nullable: true),
                    Role = table.Column<string>(nullable: true),
                    LoginId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.UserRoleId);
                    table.ForeignKey(
                        name: "FK_UserRoles_Logins_LoginId",
                        column: x => x.LoginId,
                        principalTable: "Logins",
                        principalColumn: "LoginId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Coaches",
                columns: table => new
                {
                    CoachId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    UserName = table.Column<string>(nullable: true),
                    UserRoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coaches", x => x.CoachId);
                    table.ForeignKey(
                        name: "FK_Coaches_UserRoles_UserRoleID",
                        column: x => x.UserRoleID,
                        principalTable: "UserRoles",
                        principalColumn: "UserRoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Athletes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Username = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false),
                    Weight = table.Column<int>(nullable: false),
                    Height = table.Column<int>(nullable: false),
                    StravaToken = table.Column<string>(nullable: true),
                    FitbitToken = table.Column<string>(nullable: true),
                    CoachID = table.Column<int>(nullable: false),
                    UserRoleID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Athletes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Athletes_Coaches_CoachID",
                        column: x => x.CoachID,
                        principalTable: "Coaches",
                        principalColumn: "CoachId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Athletes_UserRoles_UserRoleID",
                        column: x => x.UserRoleID,
                        principalTable: "UserRoles",
                        principalColumn: "UserRoleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Activities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ActivityDate = table.Column<string>(nullable: true),
                    ActivityType = table.Column<string>(nullable: true),
                    ActivityAmount = table.Column<int>(nullable: false),
                    Quality = table.Column<string>(nullable: true),
                    Intensity = table.Column<string>(nullable: true),
                    AthleteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Activities_Athletes_AthleteId",
                        column: x => x.AthleteId,
                        principalTable: "Athletes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Coaching",
                columns: table => new
                {
                    CoachingID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Date = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    BorderColor = table.Column<string>(nullable: true),
                    BackgroundColor = table.Column<string>(nullable: true),
                    Misc = table.Column<string>(nullable: true),
                    PrescribedEffort = table.Column<string>(nullable: true),
                    PrescribedAmount = table.Column<string>(nullable: true),
                    AthleteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Coaching", x => x.CoachingID);
                    table.ForeignKey(
                        name: "FK_Coaching_Athletes_AthleteId",
                        column: x => x.AthleteId,
                        principalTable: "Athletes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Schedules",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    date = table.Column<string>(nullable: true),
                    WorkStart = table.Column<string>(nullable: true),
                    WorkFinish = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Misc = table.Column<string>(nullable: true),
                    AthleteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schedules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Schedules_Athletes_AthleteId",
                        column: x => x.AthleteId,
                        principalTable: "Athletes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sleep",
                columns: table => new
                {
                    sleepId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    SleepDate = table.Column<string>(nullable: true),
                    SleepStart = table.Column<string>(nullable: true),
                    SleepEnd = table.Column<string>(nullable: true),
                    SleepAmount = table.Column<double>(nullable: false),
                    SleepQuality = table.Column<string>(nullable: true),
                    AthleteId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sleep", x => x.sleepId);
                    table.ForeignKey(
                        name: "FK_Sleep_Athletes_AthleteId",
                        column: x => x.AthleteId,
                        principalTable: "Athletes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activities_AthleteId",
                table: "Activities",
                column: "AthleteId");

            migrationBuilder.CreateIndex(
                name: "IX_Athletes_CoachID",
                table: "Athletes",
                column: "CoachID");

            migrationBuilder.CreateIndex(
                name: "IX_Athletes_UserRoleID",
                table: "Athletes",
                column: "UserRoleID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Coaches_UserRoleID",
                table: "Coaches",
                column: "UserRoleID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Coaching_AthleteId",
                table: "Coaching",
                column: "AthleteId");

            migrationBuilder.CreateIndex(
                name: "IX_Schedules_AthleteId",
                table: "Schedules",
                column: "AthleteId");

            migrationBuilder.CreateIndex(
                name: "IX_Sleep_AthleteId",
                table: "Sleep",
                column: "AthleteId");

            migrationBuilder.CreateIndex(
                name: "IX_UserRoles_LoginId",
                table: "UserRoles",
                column: "LoginId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activities");

            migrationBuilder.DropTable(
                name: "Coaching");

            migrationBuilder.DropTable(
                name: "Schedules");

            migrationBuilder.DropTable(
                name: "Sleep");

            migrationBuilder.DropTable(
                name: "Athletes");

            migrationBuilder.DropTable(
                name: "Coaches");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "Logins");
        }
    }
}
