﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyclingHealthTracker.Models
{
    public class UserRole
    {
        public int UserRoleId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Role { get; set; }
        public int LoginId { get; set; }
        [JsonIgnore]

        public virtual Login Login { get; set; }    //links the UserRole with LoginDetails. A userRole can only have one Login, likewise a login can only have one userRole
        public virtual Athlete Athlete { get; set; }    //links the UserRole with LoginDetails. A userRole can only have one Login, likewise a login can only have one userRole
        public virtual Coach Coach { get; set; }    //links the UserRole with LoginDetails. A userRole can only have one Login, likewise a login can only have one userRole


    }
}
