﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CyclingHealthTracker.Models
{
    public class Login



    {
        public int LoginId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }


        public virtual UserRole UserRole { get; set; }    //links the UserRole with LoginDetails. A userRole can only have one Login, likewise a login can only have one userRole

    }
}
