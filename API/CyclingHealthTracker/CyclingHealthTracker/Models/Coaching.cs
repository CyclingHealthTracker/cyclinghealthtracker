﻿using CyclingHealthTracker.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CyclingHealthTracker.Models
{ 
    public class Coaching
{
    public int CoachingID { get; set; }
    public string Date { get; set; }
    public string Title { get; set; }
    public string BorderColor { get; set; }
    public string BackgroundColor { get; set; }
    public string Misc { get; set; }
    public string PrescribedEffort { get; set; }
    public string PrescribedAmount { get; set; }
        [JsonIgnore]

        public virtual Athlete Athlete { get; set; }

    }
}