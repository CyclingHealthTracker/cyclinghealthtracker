﻿using CyclingHealthTracker.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CyclingHealthTracker.Models
{ 
    public class Athlete
{

    public int Id { get; set; }
    public string Username { get; set; }
    public int Age { get; set; }
    public int Weight { get; set; }
    public int Height { get; set; }
    public string StravaToken { get; set; }
    public string FitbitToken { get; set; }
    public int CoachID { get; set; }
    public int UserRoleID { get; set; }
    public virtual UserRole UserRole { get; set; }    //links the UserRole with LoginDetails. A userRole can only have one Login, likewise a login can only have one userRole

        public virtual ICollection<Sleep> Sleep { get; set; }
        public virtual ICollection<Activities> Activities { get; set; }
        [JsonIgnore]
        public virtual Coach Coach { get; set; }


    }
}