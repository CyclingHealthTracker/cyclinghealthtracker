﻿using CyclingHealthTracker.Database;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CyclingHealthTracker.Models
{ 
    public class Activities
{
        public int Id { get; set; }
        public string ActivityDate { get; set; }
        public string ActivityType { get; set; }
        public int ActivityAmount { get; set; }
        public string Quality { get; set; }
        public string Intensity { get; set; }
        public int AthleteId { get; set; }
            [JsonIgnore]

            public virtual Athlete Athlete { get; set; }
}
}