﻿using CyclingHealthTracker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CyclingHealthTracker.Models
{ 
    public class Sleep
{
    public int sleepId { get; set; }
    public string SleepDate { get; set; }
    public string SleepStart { get; set; }
    public string SleepEnd { get; set; }
    public double SleepAmount { get; set; }
    public string SleepQuality { get; set; }
    public virtual Athlete Athlete { get; set; }


    }
}