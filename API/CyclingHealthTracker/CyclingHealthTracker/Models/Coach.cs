﻿using CyclingHealthTracker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CyclingHealthTracker.Models
{ 
    public class Coach
{

    public int CoachId { get; set; }
    public string UserName { get; set; }
    public virtual ICollection<Athlete> Athletes { get; set; }
    public int UserRoleID { get; set; }  //each coach will have a userRoleID which will link all details to the users login details and user role. 
    public virtual UserRole UserRole { get; set; }    //links the UserRole with coach



    }
}