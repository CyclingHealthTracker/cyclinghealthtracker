﻿using CyclingHealthTracker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CyclingHealthTracker.Models
{ 
    public class Register
{
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string EmailAddress { get; set; }
        public string Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CoachEmail { get; set; }
        public int Age { get; set; }
        public int Weight { get; set; }
        public int Height { get; set; }

    }
}