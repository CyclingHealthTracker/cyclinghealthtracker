﻿using CyclingHealthTracker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace CyclingHealthTracker.Models
{ 
    public class Schedule
{
        public int Id { get; set; }
        public string date { get; set; }
        public string WorkStart { get; set; }
        public string WorkFinish { get; set; }
        public string Title { get; set; }
        public string Misc { get; set; }

        public virtual Athlete Athlete { get; set; }

    }
}