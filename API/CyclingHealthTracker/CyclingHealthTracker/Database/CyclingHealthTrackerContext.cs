﻿using CyclingHealthTracker.Models;
using Microsoft.EntityFrameworkCore;


namespace CyclingHealthTracker.Database
{
    public class CyclingHealthTrackerContext : DbContext
    {
        public DbSet<Athlete> Athletes { get; set; }
        public DbSet<Login> Logins { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Sleep> Sleep { get; set; }
        public DbSet<Activities> Activities { get; set; }
        public DbSet<Coaching> Coaching { get; set; }
        public DbSet<Schedule> Schedules { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder options)
        { 
            options.UseSqlite("Data Source=cyclingHealthTracker.db");
            options.UseLazyLoadingProxies();
        }


    }
}